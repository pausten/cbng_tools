#!/usr/bin/env python3

import  os

import  argparse
from    cbng_lib.uio import UIO
from    cbng_lib.base import Base
from    cbng_lib.platform import Platform, LinuxPlatform

class CBNGTools(Base):

    def __init__(self, uio, options):
        """@brief Constructor
           @param uio A UIO instance handling user input and output (E.G stdin/stdout or a GUI)
           @param options An instance of the OptionParser command line options."""
        super().__init__(uio, options)

        self._linuxPlatform = LinuxPlatform(self._uio, self._options)

        self._installFolder = Base.GetInstallFolder()

    def show(self):
        """@brief Show details of the tools available."""

        if Platform.IsLinux():
            cmd = "dpkg -s python-cbng-tools"
            rc, lines, _ = self._linuxPlatform.runCmd(cmd)
            if rc != 0:
                raise Exception("{} command failed.".format(cmd))

            for line in lines:
                self._uio.info(line)

            if self._options.programs:
                self._uio.info("Available commands.")
                self._uio.info("")
                entryList = os.listdir(self._installFolder)
                for entry in entryList:
                    # Ignore this command
                    if entry == "cbng_tools.py":
                        continue
                    if entry.endswith(".py"):
                        entry = entry.replace(".py", "")
                        self._uio.info(entry)

            elif self._options.cmd_line_help:
                self._uio.info("CBNG tools.")
                self._uio.info("")
                entryList = os.listdir(self._installFolder)
                for entry in entryList:
                    # Ignore this command
                    if entry == "cbng_tools.py":
                        continue
                    if entry.endswith(".py"):
                        cmd = f"python {entry} -h"
                        self._uio.info("Command: {}".format(cmd))
                        rc, lines, _ = self._linuxPlatform.runCmd(cmd)
                        if rc != 0:
                            raise Exception("{} command failed.".format(cmd))
                        for line in lines:
                            self._uio.info(line)

if __name__== '__main__':
    uio = UIO()

    parser = argparse.ArgumentParser(description="Show CBNG tools available.",
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-d", "--debug",            action='store_true', help="Enable debugging.")
    parser.add_argument("-p", "--programs",         action='store_true', help="Show all available programs.")
    parser.add_argument("-c", "--cmd_line_help",    action='store_true', help="Show all programs and the command line help text of each.")

    options = parser.parse_args()

    try:
        options = parser.parse_args()

        uio.enableDebug(options.debug)
        tools = CBNGTools(uio, options)
        tools.show()

    #If the program throws a system exit exception
    except SystemExit:
        pass
    #Don't print error information if CTRL C pressed
    except KeyboardInterrupt:
        pass
    except Exception as ex:
        raise
        if options.debug:
            raise
        else:
            uio.error( str(ex) )
