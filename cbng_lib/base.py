# Responsible for the most general methods that apply to many class types.
import os
import sys
from   pathlib import Path
from   time import time, strftime, localtime
from   p3lib.helper import logTraceBack, getHomePath

class Base(object):
    GIT_HASH_FILE      = "git_hash.txt" # This is created when the package is built.
    ASSETS_FOLDER_NAME = "assets"

    @staticmethod
    def GetGitHash():
        """@brief Get the current git hash for the installed cbng_tools package."""
        assetsFolder = Base.GetAssetsFolder()
        gitHashFile = os.path.join(assetsFolder, Base.GIT_HASH_FILE)
        if os.path.isfile(gitHashFile):
            with open(gitHashFile, 'r') as fd:
                lines = fd.readlines()
            if len(lines) > 0:
                return lines[0].strip("\r\n")
            else:
                raise Exception(f"{gitHashFile} file contains no git hash.")
        else:
            raise Exception(f"{gitHashFile} file not found.")

    @staticmethod
    def GetAssetsFolder():
        """@brief Get the assets folder location. This location holds all assets used by the tools.
           @return the assets folder"""
        file = sys.argv[0]
        pathname = os.path.dirname(file)
        assetsFolderList = [pathname + os.sep + "assets", ".." + os.sep + "assets", "assets"]
        for assetsFolder in assetsFolderList:
            if os.path.isdir(assetsFolder):
                return assetsFolder

        raise Exception("Unable to find assets folder.")

    def GetInstallFolder():
        """@brief Get the location where the CBNG tools is installed."""
        assetsFolder = Base.GetAssetsFolder()
        path = Path(assetsFolder)
        return path.parent.absolute()

    @staticmethod
    def GetLogFile(logPath, logFilePrefix, startTime=time()):
        """@brief Get the name of the UO log file to create.
           @param logPath The path that the log file sits in.
           @param logFilePrefix The start text of the log file name.
           @return The log file"""
        timeStamp = strftime("%Y-%m-%d_%H-%M-%S", localtime(startTime)).lower()
        filename = "{}_{}.txt".format(logFilePrefix, timeStamp)
        #If log folder does not exist, create it.
        if not os.path.isdir(logPath):
            os.makedirs(logPath)

        return os.path.join(logPath, filename)

    @staticmethod
    def GetHomePath():
        """@return The current users home path."""
        return getHomePath()

    def __init__(self, uio, options):
        """@brief Constructor
           @param uio A UIO instance handling user input and output (E.G stdin/stdout or a GUI)
           @param options An instance of the OptionParser command line options."""
        self._uio = uio
        self._options = options

    def info(self, msg):
        """@brief A helper method (reduces typing) to display an info level message."""
        self._uio.info(msg)

    def warn(self, msg):
        """@brief A helper method (reduces typing) to display an warning level message."""
        self._uio.warn(msg)

    def error(self, msg):
        """@brief A helper method (reduces typing) to display an error level message."""
        self._uio.error(msg)

    def debug(self, msg):
        """@brief A helper method (reduces typing) to display a debug level message."""
        self._uio.debug(msg)
