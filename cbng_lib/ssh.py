#!/usr/bin/env python3

from   p3lib.ssh import SSH as _SSH

class SSH(_SSH):

    DEFAULT_SSH_PORT = 22

    def __init__(self, address, username, password=None, port=DEFAULT_SSH_PORT, uio=None, connect=True, privateKeyFile=None, enableAutoLoginSetup=True, connectSFTPSession=False):
        """@brief Constructor to create an SSH connection.
           @param address               The address of the SSH server.
           @param username              The username to use when connecting to thge ssh server.
           @param password              The password to use in order to connect to the ssh server.
                                        Not required if the ssh server authorized_keys file has a
                                        copy of the local machines public ssh key.
           @param port                  The SSH server port.
           @param uio                   A UIO instance. If supplied then connection messages will be displayed.
                                        Can be left at the default of None. This is only really needed if you
                                        want to be able to copy the local public ssh key to the server
                                        authorized keys file as the user must be prompted to enter the
                                        servers ssh password to do this.
           @param connect               If left at the default of True then a connection attempt will be made
                                        when the constructor is called. If set to false then the caller must
                                        call the connect method.
           @param privateKeyFile        The local private key file to use.
           @param enableAutoLoginSetup  If True (default=True) and auto login is not setup the
                                        user is prompted for the password and the local ssh public key
                                        is copied to the server.
           @param connectSFTPSession    If True (default=False) then after the ssh conection is built an FTP session
                                        will be connected."""
        super().__init__(address, username, password=password, port=port, uio=uio)
        self._enableAutoLoginSetup = enableAutoLoginSetup
        self._connectSFTPSession = connectSFTPSession
        if connect:
            self._connect()

    def connect(self):
        """@brief Connect to the ssh server."""
        super().connect(enableAutoLoginSetup=self._enableAutoLoginSetup, connectSFTPSession=self._connectSFTPSession)
