

from   p3lib.uio import UIO as _UIO
from   cbng_lib.base import Base

class UIO(_UIO):

    """@brief Responsible for providing User input output functions, typically via stdi/stdout.
              This extends the p3lib UIO class so that specialised CBNG methods can be implemented."""

    def __init__(self):
        super().__init__()
        # When a UIO obj is intantiaed we always show the git hash.
        # This ensures when any test is executed the git hash of the CBNG tools used is shown in the log.
        # This gives traceability of the version of CBNG tools used for any test performed.
        gitHash = Base.GetGitHash()
        self.info(f"CBNG Tools GIT Hash: {gitHash}")
