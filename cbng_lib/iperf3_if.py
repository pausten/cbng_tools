# Responsible for providing an interface to the iperf3 throughput test tool.

import os

from   time import sleep, time

from   threading import Thread, Lock
from   queue import Queue, Empty
from   scp import SCPClient

from   p3lib.ssh import SSH
from   p3lib.uio import UIO

from   cbng_lib.constants import AMD64_ARCH_NAMES, ARMHF_ARCH_NAMES, ARM64_ARCH_NAMES
from   cbng_lib.base import Base

class IP3PPConfig(object):

    DEFAULT_SSH_PORT = 22

    ETH_SRC_ADDRESS_LENGTH      = 6
    ETH_DEST_ADDRESS_LENGTH     = ETH_SRC_ADDRESS_LENGTH
    IEEE_802_1Q_LENGTH          = 4
    ETH_TYPE_FIELD_LENGTH       = 2
    IP_HEADER_LENGTH            = 20
    UDP_HEADER_LENGTH           = 8
    ETH_FCS_LENGTH              = 4
    ETH_INTERFRAME_GAP_LENGTH   = 12
    ETH_PREAMBLE_LENGTH         = 8 
    
    """@brief Responsible for storing the iperf3 test parameters for a single point to point iperf3 test.
              This configuration is managed by IP3Manager instances."""
    def __init__(self):
        """@brief Constructor"""
        self.srcSSHHost                 = ""       # The source ssh server address for the test stream.
        self.srcSSHUsername             = ""       # The SSH username used to log into the source ssh server
        self.srcSSHPort                 = IP3PPConfig.DEFAULT_SSH_PORT # The SSH port to connect to the src end of the stream.
        self.srcTestIFAddress           = ""       # The address of the interface on the source end that data will run over.
        self.destSSHHost                = ""       # The destination ssh server address for the test stream.
        self.destSSHUsername            = ""       # The SSH username used to log into the destination ssh server
        self.destSSHPort                = IP3PPConfig.DEFAULT_SSH_PORT # The SSH port to connect to the dest end of the stream.
        self.destTestIFAddress          = ""       # The address of the interface on the destination end that data will run over.
        self.pollSeconds                = 1.0      # The frequency with which iperf3 should update throughput stats
        self.testSeconds                = 60       # The test period in seconds
        self.mbps                       = 0        # The default rate to send data 0 = max rate.
        self.udp                        = True     # Default is to perform a UDP test. If False a TCP test is performed.
        self.tos                        = 0        # The TOS value to set. Set if >= 0 and <= 255.
        self.diffSrv                    = 0        # The Diffserv value to set in the packets. Set if >= 0 and <= 63.
        self.connectTimeout             = 5000     # The connect timeout in milli seconds for the data test.
        self.streamCount                = 1        # The number of streams to run concurrently.
        self.bidir                      = False    # If True run a bi directional test.
        self.window                     = 0        # The TCP window size in bytes (0 = use system setting).
        self.mss                        = 0        # The maximum TCP segment size (0 = don't set the MSS size)
        self.startupSeconds             = 0        # The number of seconds we allow the stream to start. If the TCP throughput
                                                   # rises slowly then it can be useful to wait for the throughput to stabilise.
                                                   # This must be an integer value.
        self.serverStartupSecs          = .25      # The server startup time in seconds.
        self.rxTimeoutSeconds           = 15       # The number of seconds to elapse without receiving data before iperf3 stops trying to RX data.
        self.dontFragment               = False    # If True then don't fragment IP packets during transport.
        self.leaveIperfRunning          = False    # If True any running instances of iperf client and server will be left running when shutDown() is called.
        self.bufferLength               = 1472     # The length of the UDP or TCP payload buffer. 1472 will send a 1518 byte Ethernet frame (L2 including FCS/CRC)
        self.rateFactor                 = 1.0      # The RX rate (reported by iperf3) is multiplied by this factor to get the RX rate at the required layer (1,2 or 3)
                
    def updateTXRate(self, rateLayer, ethernetFrameSize, iee8021QEnabled):
        """@brief Update the TX rate based upon the size of the Ethernt frame and whether 802.1Q is enabled.
                  This is only done if the test is a UDP test.
            @param rateLayer Either 
                   1 = Layer 1 rate. This includes the Ethernet interframe gap and preamble.
                   2 = Layer 2/Ethernet frame rate.
                   3 = Layer 3/UDP payload rate.
            @param ethernetFrameSize The size of the Ethernet frame (L2) including the FCS/CRC.
            @param iee8021QEnabled True if 802.1Q is enabledon the interface used."""
        if self.udp:
            
            # PJA TODO 
            # This should be done inside iperf3_if.py
            # Also iperf3_if.py should multiply the RX rate by 1/txRateFactor
    
            overHead = IP3PPConfig.ETH_SRC_ADDRESS_LENGTH +\
                       IP3PPConfig.ETH_DEST_ADDRESS_LENGTH +\
                       IP3PPConfig.ETH_TYPE_FIELD_LENGTH +\
                       IP3PPConfig.IP_HEADER_LENGTH +\
                       IP3PPConfig.UDP_HEADER_LENGTH +\
                       IP3PPConfig.ETH_FCS_LENGTH
                                       
            # If 802.1Q is enabled on the Ethernet interface then an extra 4 bytes will be present in the
            # Ethernet frame. Therefore take this into account when calculating the required UDP payload size.
            if iee8021QEnabled:
                overHead += IP3PPConfig.IEEE_802_1Q_LENGTH
    
            self.bufferLength = ethernetFrameSize - overHead
    
            # Adjust the iperf3 TX rate based upon the layer at which the TX/RX rates 
            # are to be set/displayed.
            if rateLayer == 1:
                txRateFactor = (ethernetFrameSize-overHead-IP3PPConfig.ETH_INTERFRAME_GAP_LENGTH-IP3PPConfig.ETH_PREAMBLE_LENGTH)/ethernetFrameSize
    
            elif rateLayer == 2:
                txRateFactor = (ethernetFrameSize-overHead)/ethernetFrameSize
                
            elif rateLayer == 3:
                # By default the entered TX rate is the UDP payload rate.
                txRateFactor = 1.0
                
            else:
                raise Exception(f"BUG: rateLayer = {rateLayer}. Must be 1,2 or 3")
    
            # Update the UDP payload TX rate
            self.mbps = self.mbps * txRateFactor
            # 1/tx rate gives the rate factor applied to the iperf3 results..
            self.rateFactor = 1.0/txRateFactor
        
class IP3ResultParser():
    """@brief Responsible processing lines received as the iperf3 test progresses."""

    SUM                     = "SUM"                                     # Normally the id field is an integer value. If it is 'SUM' then the value is the sum of the indivitual streams.
    COMMON_TITLE_TEXT       = "Interval           Transfer     Bitrate" # This is common text in all the iperf3 column title lines.
    TEST_DONE_TEXT          = "iperf Done."                             # The text returned by iperf3 when the test is complete.
    IPERF3_ID               = 'ID'                                      # The text that appears in the ID field on the iperf3 title lines.
    IPERF3_SENDER_TEXT      = "sender"                                  # The text present in the iperf3 output for a final sender/TX result
    IPERF3_RECEIVER_TEXT    = "receiver"                                # The text present in the iperf3 output for a final receiver/RX result

    ID_KEY                                              = "ID_KEY"                                      # The ID field value. This can be a string (SUM) or an int.
    ROLE_KEY                                            = "ROLE_KEY"                                    # The iperf role field.
                                                                                                        # This is not present for single stream or non bi directional tests.
                                                                                                        # Values are
                                                                                                        # TX-C
                                                                                                        # RX-C
                                                                                                        # TX-S
                                                                                                        # RX-S

    LINE_RECEIVED_KEY                                   = "LINE_RECEIVED"                               # The line of text received from the iperf3 command.
    TEST_COMPLETE_KEY                                   = "TEST_COMPLETE"                               # True when the above line contains TEST_DONE_TEXT, False if not
    INTERVAL_START_KEY                                  = "INTERVAL_START"                              # The start time in seconds of the test interval (float)
    INTERVAL_STOP_KEY                                   = "INTERVAL_STOP"                               # The stop time in seconds of the test interval (float)

    TRANSFERRED_MB_KEY                                  = "TRANSFERRED_MB"                              # The number of MB transferred (float)
    RATE_MBPS_KEY                                       = "RATE_MBPS"                                   # The transfer rate in Mbps (float)

    UDP_RX_JITTER_KEY                                   = "UDP_RX_JITTER"                               # The UDP Jitter in ms (float)
    UDP_RX_LOST_DATAGRAM_COUNT_KEY                      = "UDP_RX_LOST_DATAGRAM_COUNT"                  # The UDP lost datagram count (float)
    UDP_RX_TOTAL_DATAGRAM_COUNT_KEY                     = "UDP_RX_TOTAL_DATAGRAM_COUNT"                 # The UDP total datagram count (float)

    TCP_TX_RETRY_COUNT_KEY                              = "TCP_TX_RETRY_COUNT"                          # The TCP TX retry count
    TCP_CWND_SIZE_KEY                                   = "TCP_CWND_SIZE"                               # The TCP Cwnd size

    SENDER_KEY                                          = "SENDER"                                      # If this key is present in the result is a final TX result
    RECEIVER_KEY                                        = "RECEIVER"                                    # If this key is present in the result is a final RX result

    ENDPOINT_ID_KEY                                     = "ENDPOINT_ID"                                 # This defines the source of the message as either the SRC or DEST endpoint.

    TX_C                                                = "TX-C"                                        # Role field when the client/source is TX'ing
    RX_C                                                = "RX-C"                                        # Role field when the client/source is RX'ing
    TX_S                                                = "TX-S"                                        # Role field when the client/source is TX'ing
    RX_S                                                = "RX-S"                                        # Role field when the client/source is RX'ing
    VALID_ROLES                                         = (TX_C, RX_C, TX_S, RX_S)                      # A list of the valid values for the role field

    IPERF3_EXIT_CODE_KEY                                = "IPERF3_EXIT_CODE"                            # If this key is present then the iperf3 command has exited.

    SRC_ENDPOINT_ID                                     = "SOURCE"                                      # The ENDPOINT_ID_KEY value when the dict holds data from the src endpoint
    DEST_ENDPOINT_ID                                    = "DESTINATION"                                 # The ENDPOINT_ID_KEY value when the dict holds data from the dest endpoint

    def __init__(self, udp):
        """@brief Constructor
           @param udp True if the test is a UDP test, False if it's a TCP test."""
        self._udp = udp

    def parse(self, line, endPointID):
        """@brief Parse a line of text output by the iperf3 tool.
           @param line A line of text received from the iperf3 tool.
           @param endPointID The ID that identifies the src of this message as either src or dest.
           @return a dict containing the filed from the line."""
        returnDict = {}
        returnDict[IP3ResultParser.ENDPOINT_ID_KEY] = endPointID
        # Save the raw line of text received from iperf3
        returnDict[IP3ResultParser.LINE_RECEIVED_KEY] = line
        pos1 = line.find("[")
        pos2 = line.find("]")
        if pos1 >= 0 and pos2 > 0:
            id = line[pos1+1:pos2].strip()
            # If not a iperf3 title line
            if id != IP3ResultParser.IPERF3_ID:
                returnDict[IP3ResultParser.ID_KEY] =id
                # Attempt to convert the id to an int. This may fail as it may contain the 'SUM' text.
                try:
                    returnDict[IP3ResultParser.ID_KEY] = int(id)
                except ValueError:
                    pass
                subLine1 = line[pos2+1:].strip()
                # Check for the role line element that is only present on bi directional tests.
                pos3 = subLine1.find("[")
                pos4 = subLine1.find("]")
                if pos3 >= 0 and pos4 > 0:
                    role = subLine1[pos3+1:pos4].strip()
                    if role in IP3ResultParser.VALID_ROLES:
                        returnDict[IP3ResultParser.ROLE_KEY]=role
                    else:
                        raise Exception("{} is not a valid role. {} are valid.".format(role, ",".join(IP3ResultParser.VALID_ROLES)))
                    # Move on to the next field to check
                    subLine1 = subLine1[pos4+1:].strip()
                subLine1Elems = subLine1.split()
                if len(subLine1Elems) > 0:
                    intervalStr = subLine1Elems[0]
                    startStopElems = intervalStr.split("-")
                    if len(startStopElems) == 2:
                        try:
                            intervalStart = float(startStopElems[0])
                            returnDict[IP3ResultParser.INTERVAL_START_KEY] = intervalStart
                            intervalStop  = float(startStopElems[1])
                            returnDict[IP3ResultParser.INTERVAL_STOP_KEY] = intervalStop

                            if len(subLine1Elems) > 2:
                                transferredMB = float(subLine1Elems[2])
                                returnDict[IP3ResultParser.TRANSFERRED_MB_KEY] = transferredMB

                                if len(subLine1Elems) > 4:
                                    multiplier=1.0
                                    # If iperf3 reports Gbits adjust multiplier as iperf3 will
                                    # report Gbits even if '-f m' cmd line options is useed.
                                    if 'Gbits/sec' in subLine1Elems:
                                        multiplier=1000.0
                                    rateMbps = float(subLine1Elems[4])*multiplier
                                    returnDict[IP3ResultParser.RATE_MBPS_KEY] = rateMbps

                                    if line.find(IP3ResultParser.IPERF3_SENDER_TEXT) > 0:
                                        returnDict[IP3ResultParser.SENDER_KEY]=True

                                    elif line.find(IP3ResultParser.IPERF3_RECEIVER_TEXT) > 0:
                                        returnDict[IP3ResultParser.RECEIVER_KEY]=True

                                    if self._udp:
                                        # If we have a TX stat that only has the total datagrams
                                        if len(subLine1Elems) == 7:
                                            totalDataGramCount = float(subLine1Elems[6])
                                            returnDict[IP3ResultParser.UDP_RX_TOTAL_DATAGRAM_COUNT_KEY]=totalDataGramCount

                                        # If we have an RX stat then we have the jitter and the total and lost datagram counts
                                        elif len(subLine1Elems) >= 9:
                                            jitter = float(subLine1Elems[6])
                                            returnDict[IP3ResultParser.UDP_RX_JITTER_KEY]=jitter
                                            lostAndTotal = subLine1Elems[8]
                                            lostAndTotalElems = lostAndTotal.split('/')
                                            if len(lostAndTotalElems) > 1:
                                                lostUDPDataGrams  = float(lostAndTotalElems[0])
                                                totalUDPDataGrams = float(lostAndTotalElems[1])
                                                returnDict[IP3ResultParser.UDP_RX_LOST_DATAGRAM_COUNT_KEY]=lostUDPDataGrams
                                                returnDict[IP3ResultParser.UDP_RX_TOTAL_DATAGRAM_COUNT_KEY]=totalUDPDataGrams

                                    # TCP test
                                    else:
                                        if len(subLine1Elems) >= 7:
                                            retryC = float(subLine1Elems[6])
                                            returnDict[IP3ResultParser.TCP_TX_RETRY_COUNT_KEY]=retryC

                                            if len(subLine1Elems) >=9:
                                                retryC = float(subLine1Elems[6])
                                                returnDict[IP3ResultParser.TCP_TX_RETRY_COUNT_KEY]=retryC
                                                cWndSize = float(subLine1Elems[7])
                                                returnDict[IP3ResultParser.TCP_CWND_SIZE_KEY]=cWndSize

                        except ValueError:
                            pass

        if line.find(IP3ResultParser.IPERF3_EXIT_CODE_KEY) >= 0:
            line = line.rstrip('\r\n')
            elems = line.split("=")
            if len(elems) == 2:
                returnDict[IP3ResultParser.IPERF3_EXIT_CODE_KEY] = float(elems[1])
        return returnDict

class IP3Manager():
    """@brief Responsible for monitoring a single point to point test using the latest version of iperf3."""
    DEPLOYMENT_FOLDER   = "/tmp"
    REMOTE_IPERF_FILE   = "{}/iperf3".format(DEPLOYMENT_FOLDER)

    MAX_STREAM_COUNT    = 10

    @staticmethod
    def IsTestFinished(iperfDictList):
        """@brief Determine if a test is complete based upon the contents of iperfDictList.
           @param iperfDictList A List or Tuple of iperfDicts as returned by getIPerfDictList().
           @return True if the test is finished."""
        # A bit of defensive programming
        if not isinstance(iperfDictList, (list, tuple)):
            raise Exception("BUG: IP3Manager.IsTestFinished() passed argument that is not a list or tuple: {}".format( str(iperfDictList) ))

        testFinished = False
        for iperfDict in iperfDictList:
            if IP3ResultParser.IPERF3_EXIT_CODE_KEY in iperfDict:
                exitCode = iperfDict[IP3ResultParser.IPERF3_EXIT_CODE_KEY]
                if exitCode == 0:
                    testFinished = True
                    break
                else:
                    raise Exception("iperf3 test completed with error code = {}".format(exitCode))
        return testFinished

    @staticmethod
    def IsFinalResult(iperfDictList):
        """@brief Determine if the results contain a final test result.
           @param iperfDictList A List if iperfDicts as returned by getIPerfDictList().
           @return True if the dicts contain final test results."""
        # A bit of defensive programming
        if not isinstance(iperfDictList, (list, tuple)):
            raise Exception("BUG: IP3Manager.IsFinalResult() passed argument that is not a list or tuple: {}".format( str(iperfDictList) ))

        finalResult = False
        for iperfDict in iperfDictList:
            if IP3ResultParser.RECEIVER_KEY in iperfDict or IP3ResultParser.SENDER_KEY in iperfDict:
                finalResult = True
                break

        return finalResult

    @staticmethod
    def IsOmitted(iperfDict):
        """@brief Determine if the results contain an ommitted (test starting up) line.
           @param iperfDict A dict containing the test results.
           @return True if the dicts contain final test results."""
        # A bit of defensive programming
        if not isinstance(iperfDict, dict):
            raise Exception("BUG: IP3Manager.IsOmitted() passed argument that is not a dict: {}".format( str(iperfDict) ))

        ommitted = False
        if IP3ResultParser.LINE_RECEIVED_KEY in iperfDict:
            lineReceived = iperfDict[IP3ResultParser.LINE_RECEIVED_KEY]
            if lineReceived.find("(omitted)") >= 0:
                ommitted = True

        return ommitted

    def __init__(self, ipTestConfig, uio=None):
        """@brief Constructor.
           @param ipTestConfig An IPTestConfig instance that defines the throughput test configuration.
           @param uio A UIO instance if user output and debug output is required."""
        self._ipTestConfig                          = ipTestConfig
        self._uio                                   = uio
        self._srcSSH                                = None
        self._destSSH                               = None
        self._ipTestResultParser                    = IP3ResultParser(ipTestConfig.udp)
        self._srcMessageQueueLock                   = Lock()
        self._srcMessageQueue                       = Queue()
        self._destMessageQueueLock                  = Lock()
        self._destMessageQueue                      = Queue()
        self._startTime                             = time()
        self._iperfDictListCache                    = []

    def _info(self, msg):
        """@brief Send an info level message to the user if a UIO instance is available."""
        if self._uio:
            self._uio.info(msg)

    def _debug(self, msg):
        """@brief Send a debug level message to the user if a UIO instance is available."""
        if self._uio:
            self._uio.debug(msg)

    def _connectSSH(self, endpointName, address, port, username, password):
        """@brief build an SSH connection to the host and check it's ok to use.
           @param address The address of the SSH connection.
           @param port The port number to the ssh connection.
           @param username The ssh server username.
           @param password The password associated with the above username."""

        self._info("Attempting to connect to the {} ssh server on {}:{}".format(endpointName, address, port))
        ssh = SSH(address, username, password=password, port=port, uio=self._uio)
        ssh.connect()
        self._info("Connected")
        self._shutdownIperf3(ssh)
        self._deployIPerf3(ssh)
        return ssh

    def _getArch(self, ssh):
        """@brief Get the platform architecture.
           @param ssh A connected SSH instance."""
        arch="unknown"
        cmd = "arch"
        _, stdOutLines, _ = self._runSSHCmd(ssh, cmd, True)
        if len(stdOutLines) > 0:
            arch = stdOutLines[0]
            arch=arch.strip("\r\n")
        return arch

    def _deployIPerf3(self, ssh):
        """@brief Deploy the correct iperf3 binary to the remote machine."""
        scp = SCPClient(ssh.getTransport())

        platformArch = self._getArch(ssh)
        self._debug("Platform Architecture: {}".format(platformArch))

        if platformArch in AMD64_ARCH_NAMES:
            binFileName = "iperf3.amd64"

        elif platformArch in ARMHF_ARCH_NAMES:
            binFileName = "iperf3.armhf"

        elif platformArch in ARM64_ARCH_NAMES:
            binFileName = "iperf3.arm64"

        else:
            raise ("{} is an unknown architecture. Failed to deploy iperf3 binary file.".format(platformArch))

        self._debug("iperf3 binary file: {}".format(binFileName))

        assetsFolder = Base.GetAssetsFolder()
        iperfFolder = os.path.join(assetsFolder, "iperf3")
        localIperfBinary = os.path.join(iperfFolder, binFileName)
        self._debug("Local iperf3 binary file: {}".format(localIperfBinary))
        if not os.path.isfile(localIperfBinary):
            raise Exception("{} file not found.".format(localIperfBinary))
        remoteIPerfBinary = IP3Manager.REMOTE_IPERF_FILE
        self._debug("Sending {} to {} on ssh server.".format(localIperfBinary, remoteIPerfBinary))
        # Deploy iperf3 binary file to ssh server
        scp.put(localIperfBinary, remoteIPerfBinary)
        self._debug("Sent {} to {} on ssh server.".format(localIperfBinary, remoteIPerfBinary))
        # Make binary executable.
        cmd = "chmod +x {}".format(remoteIPerfBinary)
        self._runSSHCmd(ssh, cmd, True)
        self._debug("Set {} as executable on ssh server. iperf3 deployment successful.".format(remoteIPerfBinary))

    def _getPID(self, psefLine):
        """@brief Get the process ID from a line of text output from a 'ps -ef' command.
           @param psefLine The line of test output by a ps -ef command.
           @return The pid (int value) or None if pid not found."""
        pid = None
        elems = psefLine.split()
        if len(elems) >= 7:
            pid = int(elems[1])
        return pid

    def _runSSHCmd(self, ssh, cmd, throwError, timeout=3):
        """@brief Run an ssh command on the ssh session.
           @param ssh The ssh connection.
           @param cmd The ssh cmd to run.
           @param throwError If True throw an error if the command fails.
           @param timeout The ssh command timeout in seconds."""
        self._debug("{}: SSH CMD: {}".format(ssh._host, cmd))
        returncode, stdOutLines, stdErrLines = ssh.runCmd(cmd, throwError=throwError, timeout=timeout)

        for line in stdOutLines:
            self._debug("{}: STDOUT SSH RESPONSE: {}".format(ssh._host, line))

        for line in stdErrLines:
            self._debug("{}: STDERR SSH RESPONSE: {}".format(ssh._host, line))

        self._debug("{}: RETURN CODE: {}".format(ssh._host, returncode))

        return (returncode, stdOutLines, stdErrLines)

    def _getPIDList(self, ssh, psefText):
        """@brief Get a list of pids that contain the psefText in the ps -ef command output.
           @param ssh The SSH instance to check for process instances.
           @param psefText The text that is expected in the ps -ef output when the process is running.
           @return A list of PID's that match."""
        pidList = []
        cmd = '/bin/ps -ef | grep "{}"'.format(psefText)
        returncode, stdOutLines, stdErrLines = self._runSSHCmd(ssh, cmd, False)
        for line in stdOutLines:
            if not line.endswith('grep "{}"'.format(psefText)) and\
               not line.endswith('grep {}'.format(psefText)):
                pid = self._getPID(line)
                if pid:
                    pidList.append(pid)
        self._debug("'{}' PID LIST: {}".format(psefText, pidList))
        return pidList

    def _shutdownIperf3(self, ssh):
        """@brief Make an attempt to shutdown any running iperf3 instances on the connected ssh connection.
                  If the iperf3 instance was started by root and the ssh connection is not root user then
                  the iperf3 instance will not be shut down.
           @param ssh A connected ssh session."""
        pidList = self._getPIDList(ssh, IP3Manager.REMOTE_IPERF_FILE)
        if len(pidList) > 0:
            for pid in pidList:
                cmd = "kill -9 {}".format(pid)
                self._runSSHCmd(ssh, cmd, False, timeout=2)

    def shutDown(self):
        """@brief Shutdown both ends of the test connection."""

        if self._srcSSH:
            # If not required to leave iperf3 instances running when the ssh connection is disconnected
            # shut them down.
            if not self._ipTestConfig.leaveIperfRunning:
                self._shutdownIperf3(self._srcSSH)

            self._srcSSH.close()
            self._srcSSH = None

        if self._destSSH:
            # If not required to leave iperf3 instances running when the ssh connection is disconnected
            # shut them down.
            if not self._ipTestConfig.leaveIperfRunning:
                self._shutdownIperf3(self._destSSH)

            self._destSSH.close()
            self._destSSH = None

    def _runIPerfServer(self):
        """@brief Run the iperf3 server on the destination end point."""
        # Start the iperf3 server so that it shuts down once the test has completed.
        cmd = "{} -s --forceflush -1".format(IP3Manager.REMOTE_IPERF_FILE)
        self._debug("Running iperf3 server: "+cmd)
        # Start the command running
        channel = self._destSSH.startCmd(cmd)
        self._startTime = time()
        self._readMessageQueueCount = 0

        while channel.recv_ready() or not channel.exit_status_ready():
            rxBytes = channel.recv(len(channel.in_buffer))
            rxData =  rxBytes.decode()
            if len(rxData) > 0:
                lines = rxData.split("\n")
                for line in lines:
                    if line and len(line) > 0:
                        # Don't add the server startup lines to the dest message queue
                        if line.find("-----------------------------------------------------------") != -1 or\
                           line.find("Server listening on") != -1:
                            continue
                        self._addToDestQueue(line)

        self._addToDestQueue("{}={}".format(IP3ResultParser.IPERF3_EXIT_CODE_KEY, channel.recv_exit_status()))

    def _runIPerfClient(self):
        """@brief Run the iperf client on the source end point."""
        cmd = IP3Manager.REMOTE_IPERF_FILE

        # Run the iperf3 client. The client runs on the destination end of the connection.
        # Therefore connect to the source from the destination.
        cmd = cmd + " -c "+self._ipTestConfig.destTestIFAddress

        # Report data rate in Mbps
        cmd = cmd + " -f m"

        # Set the reporting interval
        cmd = cmd + " -i {:.3f}".format(self._ipTestConfig.pollSeconds)

        # Force flushing at every interval
        cmd = cmd + " --forceflush"

        # By default a TCP test is performed.
        if self._ipTestConfig.udp:
            # If a UDP test is required add the argument.
            cmd = cmd + " -u"
            cmd = cmd + f" -l {self._ipTestConfig.bufferLength}"

            # The window size when running UDP tests sets the RX buffer size.
            if self._ipTestConfig.window > 0:
                # Set the window size
                cmd = cmd + " -w {}".format(int(self._ipTestConfig.window))

        # If a TCP test
        else:
            # If not using the system setting for the TCP window size.
            if self._ipTestConfig.window > 0:
                # Set the window size
                cmd = cmd + " -w {}".format(int(self._ipTestConfig.window))
            # If not using the systenm setting for the TCP maximum segment size.
            if self._ipTestConfig.mss > 0:
                # Set the TCP MSS size
                cmd = cmd + " -M {}".format(int(self._ipTestConfig.mss))

        # Set the connect timeout on the connectsetup by iperf3
        cmd = cmd + " --connect-timeout {}".format(int(self._ipTestConfig.connectTimeout))

        #Set the required bit rate.
        # If set less than 0 then set to max rate.
        if self._ipTestConfig.mbps < 0:
            self._ipTestConfig.mbps = 0
        cmd = cmd + " -b {:.6f}m".format(float(self._ipTestConfig.mbps))

        # Set the iperf pacing timer so that for every poll period iperf3 makes 4 attempts to adjust to the correct TX rate.
        # This helps to maintain a stable throughput.
        cmd = cmd + " --pacing-timer {}".format(self._ipTestConfig.pollSeconds*500)

        # Set how long the test should run in seconds.
        cmd = cmd + " -t {:.3f}".format(int(float(self._ipTestConfig.testSeconds)))

        # Set how many streams we want the test to comprise of.
        cmd = cmd + " -P {}".format(int(self._ipTestConfig.streamCount))

        if self._ipTestConfig.bidir:
            # If a bi directional test
            cmd = cmd + " --bidir"

        if self._ipTestConfig.tos >= 0 and self._ipTestConfig.tos <= 255:
            # Set the Type of Service field in the packets (0 - 255).
            cmd = cmd + " --tos {}".format(self._ipTestConfig.tos)

        if self._ipTestConfig.diffSrv >= 0 and self._ipTestConfig.diffSrv <= 63:
            # Set the diff serve field in the packets (0 - 63).
            cmd = cmd + " --dscp {}".format(self._ipTestConfig.diffSrv)

        # Use zero copy method of sending data as this uses less resources (CPU and memory)
        # No longer use -Z as found that when running bi directional TCP tests over the air
        # link on nodes the throughput was ~ 700 Mbps (MeshV2 - CPEV2) when -Z was used and
        # ~ 850 Mbps when -Z was not used.
        # cmd = cmd + " -Z"

        if self._ipTestConfig.startupSeconds > 0:
            cmd = cmd + " -O {}".format( int(self._ipTestConfig.startupSeconds) )

        # Add the RX timeout if specified in the config
        if self._ipTestConfig.rxTimeoutSeconds > 0:
            cmd = cmd + " --rcv-timeout {}".format(self._ipTestConfig.rxTimeoutSeconds*1000)

        if self._ipTestConfig.dontFragment:
            cmd = cmd + " --dont-fragment"

        cmd = cmd + " --udp-counters-64bit"

        cmd = cmd + " --repeating-payload"

        self._debug("Running iperf3 client: "+cmd)
        # Start the command running
        channel = self._srcSSH.startCmd(cmd)
        self._startTime = time()
        self._readMessageQueueCount = 0

        while channel.recv_ready() or not channel.exit_status_ready():
            rxBytes = channel.recv(len(channel.in_buffer))

            rxData =  rxBytes.decode()

            if len(rxData) > 0:
                lines = rxData.split("\n")
                for line in lines:
                    if line and len(line) > 0:
                        self._addToSrcQueue(line)

        self._addToSrcQueue("{}={}".format(IP3ResultParser.IPERF3_EXIT_CODE_KEY, channel.recv_exit_status()))

    def _addToSrcQueue(self, line):
        """@brief Add the message received from the iperf 3 src/client to the src message queue.
           @param line The line of text received from the iperf3 src client."""
        # Push messages into the src message queue under lock.
        self._srcMessageQueueLock.acquire()
        self._srcMessageQueue.put(line)
        self._srcMessageQueueLock.release()

    def _addToDestQueue(self, line):
        """@brief Add the message received from the iperf 3 dest/server to the dest message queue.
           @param line The line of text received from the iperf3 dest server."""
        # Push messages into the src message queue under lock.
        self._destMessageQueueLock.acquire()
        self._destMessageQueue.put(line)
        self._destMessageQueueLock.release()

    def start(self):
        """@brief Start a throughput test running. This method will block while building ssh
                connections to the src and the destination machines. Once connected and a few
                checks have been performed that the test should start then separate threads
                are created to monitor the test progress."""
        self._iperfDictListCache = []
        self._srcSSH = self._connectSSH("source",
                                        self._ipTestConfig.srcSSHHost,
                                        self._ipTestConfig.srcSSHPort,
                                        self._ipTestConfig.srcSSHUsername,
                                        "") # We don't allow password based login. This ensures pulic/provate keys are setup on test stations.
        self._destSSH = self._connectSSH("destination",
                                         self._ipTestConfig.destSSHHost,
                                         self._ipTestConfig.destSSHPort,
                                         self._ipTestConfig.destSSHUsername,
                                         "") # We don't allow password based login. This ensures pulic/provate keys are setup on test stations.

        if self._ipTestConfig.streamCount > IP3Manager.MAX_STREAM_COUNT:
            raise Exception("{} stream count unsupported (MAX = {}).".format(self._ipTestConfig.streamCount, IP3Manager.MAX_STREAM_COUNT))

        destThread = Thread(target=self._runIPerfServer)
        destThread.setDaemon(True)
        destThread.start()
        self._debug("Started iperf3 server running on {}:{}".format(self._ipTestConfig.destSSHHost, self._ipTestConfig.destSSHPort))
        sleep(self._ipTestConfig.serverStartupSecs)
        srcThread = Thread(target=self._runIPerfClient)
        srcThread.setDaemon(True)
        srcThread.start()
        self._debug("Started iperf3 client running on {}:{}".format(self._ipTestConfig.srcSSHHost, self._ipTestConfig.srcSSHPort))

    def _readMessageFromQueue(self, messageQueue, messageQueueLock, endPointID, readTimeout=10.0):
        """@brief Read a number of messages from the message queue if possible but if none are available
           @param messageQueue The message queue to read.
           @param messageQueueLock The threading.Lock instance to lock this thread.
           @param endPointID The ID to identify where messages read came from.
           @param readTimeout The maximum time (seconds) to wait for the expected test results on the stats queue.
           @return A dict which holds the iperf3 data read from the message in the queue or None if no messages
                   are currently available in the queue."""
        iPerf3Dict = None
        iperf3RXLine = None

        try:
            try:
                messageQueueLock.acquire()
                # If messages are available in the queue
                if messageQueue.qsize() > 0:
                    iperf3RXLine = messageQueue.get(block=True, timeout=readTimeout)
            finally:
                if messageQueueLock.locked():
                    messageQueueLock.release()
                #If we read a message from the queue
                if iperf3RXLine:
                    #Convert the line received to a dict containing the iperf3 values
                    iPerf3Dict = self._ipTestResultParser.parse(iperf3RXLine, endPointID)

        except Empty:
            self._debug("Failed to read a message from the message queue.")

            # A more accurate message may be
            #     "Failed to read all {} messages from the message queue. Read {} messages.".format(readCount, len(p2pIP3ResultParserInstanceList))
            # but as this details the internals of the iperf3 test this is possibly a better message for the user.
            raise Exception("Throughput test stopped after {:.3f} seconds.".format(time()-self._startTime))

        return iPerf3Dict

    def _getSrcMessagesAvailable(self):
        """@brief Get the number of messages available in the src message queue.
           @return the number of messages available in the src message queue."""
        self._srcMessageQueueLock.acquire()
        messagesAvailable = self._srcMessageQueue.qsize()
        self._srcMessageQueueLock.release()
        return messagesAvailable

    def _getDestMessagesAvailable(self):
        """@brief Get the number of messages available in the dest message queue.
           @return the number of messages available in the dest message queue."""
        self._destMessageQueueLock.acquire()
        messagesAvailable = self._destMessageQueue.qsize()
        self._destMessageQueueLock.release()
        return messagesAvailable

    def _isNewTestInterval(self, iperfDict):
        """@brief Determine if this is a new test interval when compared to the messages in the cache.
           @brief iperfDict The dict containing the messages received from an iperf3 instance."""
        newTestInterval = False
        # If we have at least one cached message
        if len(self._iperfDictListCache) > 0:
            # Get the start and stop intervals from this message
            startIntervalSecs = iperfDict[IP3ResultParser.INTERVAL_START_KEY]
            stopIntervalSecs = iperfDict[IP3ResultParser.INTERVAL_STOP_KEY]
            #Get the last cached message
            lastCachedIperfDict = self._iperfDictListCache[-1]
            if lastCachedIperfDict[IP3ResultParser.INTERVAL_START_KEY] != startIntervalSecs or\
               lastCachedIperfDict[IP3ResultParser.INTERVAL_STOP_KEY] != stopIntervalSecs:
                newTestInterval = True

        return newTestInterval

    def _hasTestInterval(self, iPerfDict):
        """@brief Determine if the iPerfDict contains a start and stop interval."""
        testIntervalFound = False
        if IP3ResultParser.INTERVAL_START_KEY in iPerfDict and\
           IP3ResultParser.INTERVAL_STOP_KEY in iPerfDict:
            testIntervalFound = True
        return testIntervalFound

    def _getDestMessageReadCount(self, srcIPerfDict):
        """@brief Get the number of messages to read from the destination given the message just read from the source.
           @param srcIPerfDict The iperf dict read from the source.
           @return The number of messages to read."""
        return 1

    def _readFromSourceQueue(self):
        """@brief Determine if we should read a message from the src or dest queue.
           @return True if we should read from the source queue."""
        readSource = False
        if len(self._iperfDictListCache) == 0 or len(self._iperfDictListCache) == 2:
            readSource = True

        if not readSource:
            # If we have already received a reszult that indicates the iperf test has completed.
            # Then all subsequent message must be read from the source endpoint as iperf3 forwards the server results to the client.
            for iPerfDict in self._iperfDictListCache:
                 if IP3Manager.IsFinalResult( [iPerfDict] ):
                    readSource = True
                    break

        return readSource

    def _addToIPerfDictCache(self, iPerfDict):
        """@brief Add to the iperf dict cache list.
           @return True if added to the cache."""
        added = False
        if self._ipTestConfig.streamCount > 1:
            # Currently we only include the SUM of all the streams when running multiple streams
            if IP3ResultParser.ID_KEY in iPerfDict and iPerfDict[IP3ResultParser.ID_KEY] == IP3ResultParser.SUM:
                self._iperfDictListCache.append(iPerfDict)
                added = True

        else:
            self._iperfDictListCache.append(iPerfDict)
            added = True

        if added:
            self._debug("iPerf3Dict received={}".format(iPerfDict))

        return added

    def _returnMessageCache(self, iPerfDict):
        """@brief Determine if we are ready to return the message cache to the caller.
           @param iPerfDict The dict received from iperf command.
           @return True if the message cache should be returned to the caller of the method."""
        returnNow = False
        # If the test is complete
        if IP3ResultParser.IPERF3_EXIT_CODE_KEY in iPerfDict:
            self._iperfDictListCache.append(iPerfDict)
            returnNow = True

        # We're only interested in results that contain a test interval
        elif self._hasTestInterval(iPerfDict) and not IP3Manager.IsOmitted(iPerfDict):

            # If we're receiving final test status messages
            if IP3Manager.IsFinalResult( [iPerfDict] ):
                # When iperf3 fnishes the source client receives the final results from the dest server.
                # Therefore we read all the test complete messages from the source.
                if IP3ResultParser.ENDPOINT_ID_KEY in iPerfDict and iPerfDict[IP3ResultParser.ENDPOINT_ID_KEY] == IP3ResultParser.SRC_ENDPOINT_ID:

                    if self._ipTestConfig.bidir:
                        #If this is a final message from the destination forwarded by iperf3 to the src
                        if IP3ResultParser.RECEIVER_KEY in iPerfDict and\
                           iPerfDict[IP3ResultParser.RECEIVER_KEY] and\
                           IP3ResultParser.ROLE_KEY in iPerfDict and\
                           iPerfDict[IP3ResultParser.ROLE_KEY] == IP3ResultParser.TX_C:
                            # Set flags to indicate this message came from the dest end and change the role to the RX-S
                           iPerfDict[IP3ResultParser.ENDPOINT_ID_KEY] = IP3ResultParser.DEST_ENDPOINT_ID
                           iPerfDict[IP3ResultParser.ROLE_KEY] = IP3ResultParser.RX_S

                        #If this is a final message from the destination forwarded by iperf3 to the src
                        if IP3ResultParser.SENDER_KEY in iPerfDict and\
                           iPerfDict[IP3ResultParser.SENDER_KEY] and\
                           IP3ResultParser.ROLE_KEY in iPerfDict and\
                           iPerfDict[IP3ResultParser.ROLE_KEY] == IP3ResultParser.RX_C:
                           # Set flags to indicate this message came from the dest end and change the role to the TX-S
                           iPerfDict[IP3ResultParser.ENDPOINT_ID_KEY] = IP3ResultParser.DEST_ENDPOINT_ID
                           iPerfDict[IP3ResultParser.ROLE_KEY] = IP3ResultParser.TX_S

                    # If a uni dir test
                    else:
                        #If this is a final message from the destination forwarded by iperf3 to the src
                        if IP3ResultParser.RECEIVER_KEY in iPerfDict and\
                           iPerfDict[IP3ResultParser.RECEIVER_KEY]:
                            # Set flag to indicate this message came from the dest
                            iPerfDict[IP3ResultParser.ENDPOINT_ID_KEY] = IP3ResultParser.DEST_ENDPOINT_ID

                    self._addToIPerfDictCache(iPerfDict)
                    # Bi directional tests should have 4 message (SRC and DEST TX and RX)
                    if self._ipTestConfig.bidir:
                        if len(self._iperfDictListCache) >= 4:
                            returnNow = True
                    # Uni directional tests should have 2 messages (SRC TX and DEST RX).
                    else:
                        if len(self._iperfDictListCache) >= 2:
                            returnNow = True

            else:
                added = self._addToIPerfDictCache(iPerfDict)
                if added:
                    if self._ipTestConfig.bidir:
                        if len(self._iperfDictListCache) >= 4:
                            returnNow = True
                    else:
                        if len(self._iperfDictListCache) >= 2:
                            returnNow = True

        return returnNow

    def getIPerfDictList(self):
        """@brief Attempt to get a list of dicts containing the iperf3 results for a current test.
                  This method should not block.
           @return None if no data is currently available or a list of dictionaries that contain the test results, during and at the end the end of the test."""
        returnIPerfDictList = []

        # Attempt to read single message from a queue under lock.
        if self._readFromSourceQueue():
            self._debug("Attempt to read 1 message from SRC message queue.")
            iPerfDict = self._readMessageFromQueue(self._srcMessageQueue, self._srcMessageQueueLock, IP3ResultParser.SRC_ENDPOINT_ID)
        else:
            self._debug("Attempt to read 1 message from DEST message queue.")
            iPerfDict = self._readMessageFromQueue(self._destMessageQueue, self._destMessageQueueLock, IP3ResultParser.DEST_ENDPOINT_ID)
        # If we have built up the messages required to return a set of dicts to the user that details the TX from the src and RX from the dest endpoints.
        if iPerfDict and self._returnMessageCache(iPerfDict):
            returnIPerfDictList = self._iperfDictListCache
            self._iperfDictListCache = []

        return returnIPerfDictList

class IP3Output(object):
    """@brief Responsible for displaying the IPerf3 output."""

    CSV_COL_TITLE_PREFIX = "#"

    def __init__(self, uio, biDir, udp, streamCount, showColor=True, rateFactor=1.0):
        """@brief Constructor
           @param uio A UIO instance handling user input and output (E.G stdin/stdout or a GUI).
           @param biDir True if the test is a bi directional test.
           @param udp True if the test is a UDP test. False = TCP test.
           @param streamCount The number of streams configured.
           @param showColor If True then if TCP re transmissions or UDP packet loss counters are not zero then these are displayed in red.
           @param rateFactor The rate adjustment factor. iperf3 reports the payload rate. This is used to report L1 and L2 rates when a UDP test is running."""
        self._uio           = uio
        self._biDir         = biDir
        self._udp           = udp
        self._showColor     = showColor
        self._streamCount   = streamCount
        self._colCharCount  = 0
        self._csvLogFileName = None
        self._rateFactor     = rateFactor

    @staticmethod
    def GetPercentageBad(good, bad):
        """@brief calc the percentage bad units.
           @param good The number of good units.
           @param bad The number of bad units.
           @return The percentage of bad units."""
        percentageBad = 0.0
        if good > 0:
            percentageBad = (bad / good) * 100.0
        return percentageBad

    def showTableHeader(self):
        """@brief Show the test table header."""
        # If bi a directional test
        if self._biDir:
            if self._udp:
                self._colCharCount = 143
                self._showTableHorLine()
                self._uio.info("|         |        |        |           | Src RX    | Src RX    | Src RX   |         |         |           | Dest RX   | Dest RX   | Dest RX  |")
                self._uio.info("|         | Src TX | Src RX | Src RX    | Lost      | Total     | Datagram | Dest TX | Dest RX | Dest RX   | Lost      | Total     | Datagram |")
                self._uio.info("| Seconds | Mbps   | Mbps   | Jitter ms | Datagrams | Datagrams | Loss %   | Mbps    | Mbps    | Jitter ms | Datagrams | Datagrams | Loss %   |")
                self._showTableHorLine()
                self._updateCSVHeader(["Seconds",
                                 "Src TX Mbps",
                                 "Src RX Mbps",
                                 "Src RX Jitter ms",
                                 "Src RX Lost Datagrams",
                                 "Src RX Total Datagrams",
                                 "Src RX Datagram Loss %",
                                 "Dest TX Mbps",
                                 "Dest RX Mbps",
                                 "Dest RX Jitter ms",
                                 "Dest RX Lost Datagrams",
                                 "Dest RX Total Datagrams",
                                 "Dest RX Datagram Loss %"])
            else:
                self._colCharCount = 97
                self._showTableHorLine()
                self._uio.info("|         | Src TX | Src TX  | Src TX      | Src RX | Dest TX | Dest TX | Dest TX     | Dest RX |")
                self._uio.info("| Seconds | Mbps   | Retries | CWnd KBytes | Mbps   | Mbps    | Retries | CWnd KBytes | Mbps    |")
                self._showTableHorLine()
                self._updateCSVHeader(["Seconds",
                                 "Src TX Mbps",
                                 "Src TX Retries",
                                 "Src TX CWnd KBytes",
                                 "Src RX Mbps",
                                 "Dest TX Mbps",
                                 "Dest TX Retries",
                                 "Dest TX CWnd KBytes",
                                 "Dest RX Mbps"])
        # If uni directional test
        else:
            if self._udp:
                self._colCharCount = 77
                self._showTableHorLine()
                self._uio.info("|         |        |         |           | Dest RX   | Dest RX   | Dest RX  |")
                self._uio.info("|         | Src TX | Dest RX | Dest RX   | Lost      | Total     | Datagram |")
                self._uio.info("| Seconds | Mbps   | Mbps    | Jitter ms | Datagrams | Datagrams | Loss %   |")
                self._showTableHorLine()
                self._updateCSVHeader(["Seconds",
                                 "Src TX Mbps",
                                 "Dest RX Mbps",
                                 "Dest RX Jitter ms",
                                 "Dest RX Lost Datagrams",
                                 "Dest RX Total Datagrams",
                                 "Dest RX Datagram Loss %"])
            # If a TCP test
            else:
                self._colCharCount = 54
                self._showTableHorLine()
                self._uio.info("|         | Src TX | Src TX  | Src TX      | Dest RX |")
                self._uio.info("| Seconds | Mbps   | Retries | CWnd KBytes | Mbps    |")
                self._showTableHorLine()
                self._updateCSVHeader(["Seconds",
                                 "Src TX Mbps",
                                 "Src TX Retries",
                                 "Src TX CWnd KBytes",
                                 "Dest RX Mbps"])

    def _getCSVFileHandle(self):
        """@brief As it says on the tin.
           @return A handle to the open file."""
        if self._csvLogFileName is None:
            self._csvLogFileName = self._uio.getLogFile()+".csv"

        if os.path.isfile(self._csvLogFileName):
            fd = open(self._csvLogFileName, 'a')
        else:
            fd = open(self._csvLogFileName, 'w')
        return fd

    def _updateCSVHeader(self, colNames):
        """@brief Update the header line in the CSV file.
           @param colNames A list of the column names."""
        fd = self._getCSVFileHandle()
        fd.write(IP3Output.CSV_COL_TITLE_PREFIX + ",".join(colNames)+"\n")
        fd.close()

    def _updateCSVBody(self, colValues):
        """@brief Update the header line in the CSV file.
           @param colNames A list of the column names."""
        fd = self._getCSVFileHandle()
        newColValues = []
        # We may get escape sequences in the output to stdout. Ensure we strip this.
        for colValue in colValues:
            newColValues.append( UIO.RemoveEscapeSeq(colValue) )
        fd.write(",".join(newColValues)+"\n")
        fd.close()

    def _showTableHorLine(self):
        """@brief Show the line that is the width of the output table."""
        self._uio.info("-"*self._colCharCount)

    def _getIperf3Dict(self, source, iperfDictList):
        """@brief Get the first iperf dict that holds the source/dest endpoint data.
           @param source If True get the source endpoint iperf3 dict.
           @param iperfDictList The list of IPerf dict instances containing the test results.
           @return The dict containing the endpoint data."""
        if source:
            requiredEndpointIDValue = IP3ResultParser.SRC_ENDPOINT_ID
        else:
            requiredEndpointIDValue = IP3ResultParser.DEST_ENDPOINT_ID

        iPerf3DictFound = None
        for iperfDict in iperfDictList:
            if IP3ResultParser.ENDPOINT_ID_KEY in iperfDict and iperfDict[IP3ResultParser.ENDPOINT_ID_KEY] == requiredEndpointIDValue:
                iPerf3DictFound = iperfDict
                break

        if not iPerf3DictFound:
            raise Exception("{} endpoint iperf3 dict not found: {}".format(requiredEndpointIDValue, iperfDictList))

        return iPerf3DictFound

    def _getDictValue(self, key, theDict):
        """@brief Get the value from a dict.
           @param key The key of the value required.
           @param theDict The dictionary to read from.
           @return The value from the dict."""
        if key in theDict:
            value = theDict[key]
            # If we have a rate then apply the required adjustment
            if key == IP3ResultParser.RATE_MBPS_KEY:
                value = value * self._rateFactor
            return value
        
        raise Exception("{} not found in the dict: {}".format(key, theDict))

    def _getUniTCPResultList(self, iperfDictList, finalResult):
        """@brief Get the list of values to display during a uni directional TCP test.
           @param iperfDictList The list of IPerf dict instances containing the test results.
           @param finalResult True if we want a final result rather than an in progress result.
           @return A list of values in the order displayed to the user."""
        valueList = []
        srcIPerf3Dict   = self._getIperf3Dict(True, iperfDictList)
        destIPerf3Dict  = self._getIperf3Dict(False, iperfDictList)
        if finalResult:
            valueList.append( "Result " )
            valueList.append( "{: <6.1f}".format( self._getDictValue(IP3ResultParser.RATE_MBPS_KEY, srcIPerf3Dict)) )
            valueList.append( " "*7 )
            valueList.append( " "*11 )
            valueList.append( "{: <7.1f}".format( self._getDictValue(IP3ResultParser.RATE_MBPS_KEY, destIPerf3Dict)) )
        else:
            valueList.append( "{: <7.0f}".format( self._getDictValue(IP3ResultParser.INTERVAL_STOP_KEY, srcIPerf3Dict)) )
            valueList.append( "{: <6.1f}".format( self._getDictValue(IP3ResultParser.RATE_MBPS_KEY, srcIPerf3Dict)) )
            # We need to allow for adding red error text on the TX retry column
            txRetryStr = "{: <7.0f}".format( self._getDictValue(IP3ResultParser.TCP_TX_RETRY_COUNT_KEY, srcIPerf3Dict))
            valueList.append( self._getZValueString(txRetryStr))
            # Multiple TCP streams have a Cwnd size on each stream so we don't display them here.
            if self._streamCount > 1:
                valueList.append( " "*11)
            else:
                valueList.append( "{: <11.0f}".format( self._getDictValue(IP3ResultParser.TCP_CWND_SIZE_KEY, srcIPerf3Dict)) )
            valueList.append( "{: <7.1f}".format( self._getDictValue(IP3ResultParser.RATE_MBPS_KEY, destIPerf3Dict)) )
        return valueList

    def _showUniTCPRow(self, iperfDictList, finalResult):
        """@brief Show the uni dir TCP results Row.
           @param iperfDictList A list of dict instances detailing the current state of the test.
           @param finalResult If True the above contains a final set of results."""
        resultList = self._getUniTCPResultList(iperfDictList, finalResult)
        self._uio.info("| {} | {} | {} | {} | {} |".format(resultList[0],
                                                           resultList[1],
                                                           resultList[2],
                                                           resultList[3],
                                                           resultList[4]))
        self._updateCSVBody(resultList)

    def _getUniUDPResultList(self, iperfDictList, finalResult):
        """@brief Get the list of values to display during a uni directional UDP test.
           @param iperfDictList The list of IPerf dict instances containing the test results.
           @param finalResult True if we want a final result rather than an in progress result.
           @return A list of values in the order displayed to the user."""
        valueList = []
        srcIPerf3Dict   = self._getIperf3Dict(True, iperfDictList)
        destIPerf3Dict  = self._getIperf3Dict(False, iperfDictList)
        if finalResult:
            valueList.append( "Result " )

        else:
            valueList.append( "{: <7.0f}".format( self._getDictValue(IP3ResultParser.INTERVAL_STOP_KEY, srcIPerf3Dict)) )

        valueList.append( "{: <6.1f}".format( self._getDictValue(IP3ResultParser.RATE_MBPS_KEY, srcIPerf3Dict)) )
        valueList.append( "{: <7.1f}".format( self._getDictValue(IP3ResultParser.RATE_MBPS_KEY, destIPerf3Dict)) )
        valueList.append( "{: <9.1f}".format( self._getDictValue(IP3ResultParser.UDP_RX_JITTER_KEY, destIPerf3Dict)) )

        lostDataGramCount = self._getDictValue(IP3ResultParser.UDP_RX_LOST_DATAGRAM_COUNT_KEY, destIPerf3Dict)
        # We need to allow for adding red error text on the lost datagrams column
        lostDataGramsStr = "{: <9.0f}".format( self._getDictValue(IP3ResultParser.UDP_RX_LOST_DATAGRAM_COUNT_KEY, destIPerf3Dict))
        # We need to allow for adding red error text on the lost datagrams column at the end of the test
        if finalResult:
            valueList.append( self._getZValueString(lostDataGramsStr))
        else:
            valueList.append(lostDataGramsStr)

        totalDataGramCount = self._getDictValue(IP3ResultParser.UDP_RX_TOTAL_DATAGRAM_COUNT_KEY, destIPerf3Dict)
        percentageLoss = IP3Output.GetPercentageBad(totalDataGramCount, lostDataGramCount)
        valueList.append( "{: <9.0f}".format(totalDataGramCount) )

        # We need to allow for adding red error text on the lost datagrams column
        percentageDataLostStr = "{: <8.4f}".format( percentageLoss )
        # We need to allow for adding red error text on the lost datagrams column at the end of the test
        if finalResult:
            valueList.append( self._getZValueString(percentageDataLostStr))
        else:
            valueList.append(percentageDataLostStr)

        return valueList

    def _showUniUDPRow(self, iperfDictList, finalResult):
        """@brief Show the uni dir UDP results Row.
           @param iperfDictList A list of dict instances detailing the current state of the test.
           @param finalResult If True the above contains a final set of results."""
        resultList = self._getUniUDPResultList(iperfDictList, finalResult)
        self._uio.info("| {} | {} | {} | {} | {} | {} | {} |".format(resultList[0],
                                                                     resultList[1],
                                                                     resultList[2],
                                                                     resultList[3],
                                                                     resultList[4],
                                                                     resultList[5],
                                                                     resultList[6]))
        self._updateCSVBody(resultList)

    def _getZValueString(self, valueStr):
        """@brief Get the string to display if the value is > 0.
           @param valueStr A string containing a number.
           @return """
        returnStr = valueStr
        if self._showColor:
            try:
                value = float(valueStr)
                # Add a VT100 escape seq to set the colour of the text if we have a value > 0
                if value > 0:
                    returnStr = UIO.GetWarnEscapeSeq() + valueStr + UIO.DISPLAY_RESET_ESCAPE_SEQ
            except ValueError:
                pass
        return returnStr

    def showProgress(self, iperfDictList):
        """@brief Show the test progress before and after completion.
           @param iperfDictList A list of dict instances detailing the current state of the test."""
        if IP3Manager.IsFinalResult(iperfDictList):
            self._showFinalResult(iperfDictList)
        else:
            self._showRunningProgress(iperfDictList)

    def _getIperf3Dicts(self, source, iperfDictList):
        """@brief Get the a pair of iperf dicts that hold either the source or dest endpoint data during the test.
           @param source If True get the source endpoint iperf3 dicts.
           @param iperfDictList The list of IPerf dict instances containing the test results.
           @return A list holding the TX and the the RX dicts containing the endpoint data."""
        if source:
            requiredEndpointIDValue = IP3ResultParser.SRC_ENDPOINT_ID
            txRole = IP3ResultParser.TX_C
            rxRole = IP3ResultParser.RX_C
        else:
            requiredEndpointIDValue = IP3ResultParser.DEST_ENDPOINT_ID
            txRole = IP3ResultParser.TX_S
            rxRole = IP3ResultParser.RX_S

        # Add the TX dict
        iPerf3DictsFound = []
        for iperfDict in iperfDictList:
            if IP3ResultParser.ENDPOINT_ID_KEY in iperfDict and\
              iperfDict[IP3ResultParser.ENDPOINT_ID_KEY] == requiredEndpointIDValue and\
              IP3ResultParser.ROLE_KEY in iperfDict and\
              iperfDict[IP3ResultParser.ROLE_KEY] == txRole:
                iPerf3DictsFound.append(iperfDict)
                break

        # Add the RX
        for iperfDict in iperfDictList:
            if IP3ResultParser.ENDPOINT_ID_KEY in iperfDict and\
              iperfDict[IP3ResultParser.ENDPOINT_ID_KEY] == requiredEndpointIDValue and\
              IP3ResultParser.ROLE_KEY in iperfDict and\
              iperfDict[IP3ResultParser.ROLE_KEY] == rxRole:
                iPerf3DictsFound.append(iperfDict)
                break

        if len(iPerf3DictsFound) != 2:
            raise Exception("Expected two iperf3 dicts but found {} endpoint iperf3 dict not found: {}".format(len(iPerf3DictsFound), iperfDictList))

        return iPerf3DictsFound

    def _getFinalIperf3Dicts(self, source, iperfDictList):
        """@brief Get the a pair of iperf dicts that hold either the source or dest endpoint data at the end of the test.
           @param source If True get the source endpoint iperf3 dicts.
           @param iperfDictList The list of IPerf dict instances containing the test results.
           @return A list holding the TX and the the RX dicts containing the endpoint data."""
        if source:
            requiredEndpointIDValue = IP3ResultParser.SRC_ENDPOINT_ID
        else:
            requiredEndpointIDValue = IP3ResultParser.DEST_ENDPOINT_ID

        # Add the TX dict
        iPerf3DictsFound = []
        for iperfDict in iperfDictList:
            if IP3ResultParser.ENDPOINT_ID_KEY in iperfDict and\
              iperfDict[IP3ResultParser.ENDPOINT_ID_KEY] == requiredEndpointIDValue and\
              IP3ResultParser.SENDER_KEY in iperfDict and\
              iperfDict[IP3ResultParser.SENDER_KEY]:
                iPerf3DictsFound.append(iperfDict)
                break

        # Add the RX
        for iperfDict in iperfDictList:
            if IP3ResultParser.ENDPOINT_ID_KEY in iperfDict and\
              iperfDict[IP3ResultParser.ENDPOINT_ID_KEY] == requiredEndpointIDValue and\
              IP3ResultParser.RECEIVER_KEY in iperfDict and\
              iperfDict[IP3ResultParser.RECEIVER_KEY]:
                iPerf3DictsFound.append(iperfDict)
                break

        if len(iPerf3DictsFound) != 2:
            raise Exception("Expected two iperf3 dicts but found {} endpoint iperf3 dict not found: {}".format(len(iPerf3DictsFound), iperfDictList))

        return iPerf3DictsFound

    def _getBiTCPResultList(self, iperfDictList, finalResult):
        """@brief Get the list of values to display during a bi directional TCP test.
           @param iperfDictList The list of IPerf dict instances containing the test results.
           @param finalResult True if we want a final result rather than an in progress result.
           @return A list of values in the order displayed to the user."""
        valueList = []
        if finalResult:
            srcTXIPerf3Dict, srcRXIPerf3Dict    = self._getFinalIperf3Dicts(True, iperfDictList)
            destTXIPerf3Dict, destRXIPerf3Dict  = self._getFinalIperf3Dicts(False, iperfDictList)

        else:
            srcTXIPerf3Dict, srcRXIPerf3Dict    = self._getIperf3Dicts(True, iperfDictList)
            destTXIPerf3Dict, destRXIPerf3Dict  = self._getIperf3Dicts(False, iperfDictList)

        if finalResult:
            valueList.append( "Result " )
        else:
            valueList.append( "{: <7.0f}".format( self._getDictValue(IP3ResultParser.INTERVAL_STOP_KEY, srcTXIPerf3Dict)) )

        valueList.append( "{: <6.1f}".format( self._getDictValue(IP3ResultParser.RATE_MBPS_KEY, srcTXIPerf3Dict)) )

        # We need to allow for adding red error text on the TX retry column
        txRetryStr = "{: <7.0f}".format( self._getDictValue(IP3ResultParser.TCP_TX_RETRY_COUNT_KEY, srcTXIPerf3Dict))
        valueList.append( self._getZValueString(txRetryStr))

        if IP3ResultParser.TCP_CWND_SIZE_KEY in srcTXIPerf3Dict:
            valueList.append( "{: <11.0f}".format( self._getDictValue(IP3ResultParser.TCP_CWND_SIZE_KEY, srcTXIPerf3Dict)) )
        else:
            valueList.append( "           ")
        valueList.append( "{: <6.1f}".format( self._getDictValue(IP3ResultParser.RATE_MBPS_KEY, srcRXIPerf3Dict)) )

        valueList.append( "{: <7.0f}".format( self._getDictValue(IP3ResultParser.RATE_MBPS_KEY, destTXIPerf3Dict)) )

        # We need to allow for adding red error text on the TX retry column
        txRetryStr = "{: <7.0f}".format( self._getDictValue(IP3ResultParser.TCP_TX_RETRY_COUNT_KEY, destTXIPerf3Dict))
        valueList.append( self._getZValueString(txRetryStr))

        if IP3ResultParser.TCP_CWND_SIZE_KEY in destTXIPerf3Dict:
            valueList.append( "{: <11.0f}".format( self._getDictValue(IP3ResultParser.TCP_CWND_SIZE_KEY, destTXIPerf3Dict)) )
        else:
            valueList.append( "           ")
        valueList.append( "{: <7.1f}".format( self._getDictValue(IP3ResultParser.RATE_MBPS_KEY, destRXIPerf3Dict)) )

        return valueList

    def _showBiTCPRow(self, iperfDictList, finalResult):
        """@brief Show the bi dir TCP results Row.
           @param iperfDictList A list of dict instances detailing the current state of the test.
           @param finalResult If True the above contains a final set of results."""
        resultList = self._getBiTCPResultList(iperfDictList, finalResult)
        self._uio.info("| {} | {} | {} | {} | {} | {} | {} | {} | {} |".format(resultList[0],
                                                                               resultList[1],
                                                                               resultList[2],
                                                                               resultList[3],
                                                                               resultList[4],
                                                                               resultList[5],
                                                                               resultList[6],
                                                                               resultList[7],
                                                                               resultList[8]))
        self._updateCSVBody(resultList)

    def _getBiUDPResultList(self, iperfDictList, finalResult):
        """@brief Get the list of values to display during a bi directional UDP test.
           @param iperfDictList The list of IPerf dict instances containing the test results.
           @param finalResult True if we want a final result rather than an in progress result.
           @return A list of values in the order displayed to the user."""
        valueList = []
        if finalResult:

            srcTXIPerf3Dict, srcRXIPerf3Dict    = self._getFinalIperf3Dicts(True, iperfDictList)
            destTXIPerf3Dict, destRXIPerf3Dict  = self._getFinalIperf3Dicts(False, iperfDictList)
        else:
            srcTXIPerf3Dict, srcRXIPerf3Dict    = self._getIperf3Dicts(True, iperfDictList)
            destTXIPerf3Dict, destRXIPerf3Dict  = self._getIperf3Dicts(False, iperfDictList)

        if finalResult:
            valueList.append( "Result " )
        else:
            valueList.append( "{: <7.0f}".format( self._getDictValue(IP3ResultParser.INTERVAL_STOP_KEY, srcTXIPerf3Dict)) )

        valueList.append( "{: <6.1f}".format( self._getDictValue(IP3ResultParser.RATE_MBPS_KEY, srcTXIPerf3Dict)) )
        valueList.append( "{: <6.1f}".format( self._getDictValue(IP3ResultParser.RATE_MBPS_KEY, srcRXIPerf3Dict)) )
        valueList.append( "{: <9.3f}".format( self._getDictValue(IP3ResultParser.UDP_RX_JITTER_KEY, srcRXIPerf3Dict)) )

        lostDataGramCount = self._getDictValue(IP3ResultParser.UDP_RX_LOST_DATAGRAM_COUNT_KEY, srcRXIPerf3Dict)
        lostDataGramsStr = "{: <9.0f}".format( self._getDictValue(IP3ResultParser.UDP_RX_LOST_DATAGRAM_COUNT_KEY, srcRXIPerf3Dict))
        # We need to allow for adding red error text on the lost datagrams column at the end of the test
        if finalResult:
            valueList.append( self._getZValueString(lostDataGramsStr))
        else:
            #During the UDP test we may have lost datagrams because they havn't ariv3ed in this second.
            #Therefore they are not marked as red during the test.
            valueList.append(lostDataGramsStr)

        totalDataGramCount = self._getDictValue(IP3ResultParser.UDP_RX_TOTAL_DATAGRAM_COUNT_KEY, srcRXIPerf3Dict)
        percentageLoss = IP3Output.GetPercentageBad(totalDataGramCount, lostDataGramCount)
        valueList.append( "{: <9.0f}".format(totalDataGramCount) )

        # We need to allow for adding red error text on the lost datagrams column
        percentageDataLostStr = "{: <8.4f}".format( percentageLoss )
        # We need to allow for adding red error text on the lost datagrams column at the end of the test
        if finalResult:
            valueList.append( self._getZValueString(percentageDataLostStr))
        else:
            valueList.append(percentageDataLostStr)

        valueList.append( "{: <7.1f}".format( self._getDictValue(IP3ResultParser.RATE_MBPS_KEY, destTXIPerf3Dict)) )
        valueList.append( "{: <7.1f}".format( self._getDictValue(IP3ResultParser.RATE_MBPS_KEY, destRXIPerf3Dict)) )
        valueList.append( "{: <9.3f}".format( self._getDictValue(IP3ResultParser.UDP_RX_JITTER_KEY, destRXIPerf3Dict)) )

        lostDataGramCount = self._getDictValue(IP3ResultParser.UDP_RX_LOST_DATAGRAM_COUNT_KEY, destRXIPerf3Dict)
        # We need to allow for adding red error text on the lost datagrams column
        lostDataGramsStr = "{: <9.0f}".format( self._getDictValue(IP3ResultParser.UDP_RX_LOST_DATAGRAM_COUNT_KEY, destRXIPerf3Dict))
        # We need to allow for adding red error text on the lost datagrams column at the end of the test
        if finalResult:
            valueList.append( self._getZValueString(lostDataGramsStr))
        else:
            valueList.append(lostDataGramsStr)

        totalDataGramCount = self._getDictValue(IP3ResultParser.UDP_RX_TOTAL_DATAGRAM_COUNT_KEY, destRXIPerf3Dict)
        percentageLoss = IP3Output.GetPercentageBad(totalDataGramCount, lostDataGramCount)
        valueList.append( "{: <9.0f}".format(totalDataGramCount) )

        # We need to allow for adding red error text on the lost datagrams column
        percentageDataLostStr = "{: <8.4f}".format( percentageLoss )
        # We need to allow for adding red error text on the lost datagrams column at the end of the test
        if finalResult:
            valueList.append( self._getZValueString(percentageDataLostStr))
        else:
            valueList.append(percentageDataLostStr)

        return valueList

    def _showBiUDPRow(self, iperfDictList, finalResult):
        """@brief Show the bi dir UDP results Row.
           @param iperfDictList A list of dict instances detailing the current state of the test.
           @param finalResult If True the above contains a final set of results."""
        resultList = self._getBiUDPResultList(iperfDictList, finalResult)
        self._uio.info("| {} | {} | {} | {} | {} | {} | {} | {} | {} | {} | {} | {} | {} |".format(resultList[0],
                                                                                                   resultList[1],
                                                                                                   resultList[2],
                                                                                                   resultList[3],
                                                                                                   resultList[4],
                                                                                                   resultList[5],
                                                                                                   resultList[6],
                                                                                                   resultList[7],
                                                                                                   resultList[8],
                                                                                                   resultList[9],
                                                                                                   resultList[10],
                                                                                                   resultList[11],
                                                                                                   resultList[12]))
        self._updateCSVBody(resultList)

    def _showFinalResult(self, iperfDictList):
        """@brief Show the final set of results for a test.
           @param iperfDictList A list of dict instances detailing the current state of the test."""
        self._showTableHorLine()
        # If bi a directional test was performed.
        if self._biDir:
            if self._udp:
                self._showBiUDPRow(iperfDictList, True)
            # If a TCP test
            else:
                self._showBiTCPRow(iperfDictList, True)
        # If bi a uni directional test was performed.
        else:
            if self._udp:
                self._showUniUDPRow(iperfDictList, True)
            # If a TCP test
            else:
                self._showUniTCPRow(iperfDictList, True)

        self._showTableHorLine()

    def _showRunningProgress(self, iperfDictList):
        """@brief Show the test progress before completion.
           @param iperfDictList A list of dict instances detailing the current state of the test."""
        # If bi a directional test was performed.
        if self._biDir:
            if self._udp:
                self._showBiUDPRow(iperfDictList, False)
            # If a TCP test
            else:
                self._showBiTCPRow(iperfDictList, False)
        # If bi a uni directional test was performed.
        else:
            if self._udp:
                self._showUniUDPRow(iperfDictList, False)
            # If a TCP test
            else:
                self._showUniTCPRow(iperfDictList, False)
