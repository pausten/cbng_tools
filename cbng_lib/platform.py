# @brief Responsible for providing platform (Linux/ Windows) functionality.

import platform
from cbng_lib.base import Base
from subprocess import Popen, PIPE

class Platform(Base):
    @staticmethod
    def IsWindows():
        """@return Determine if running on a Windows platform."""
        osName = platform.system()
        windows = False
        if osName == "Windows":
            windows = True

        return windows

    @staticmethod
    def IsLinux():
        """@return Determine if running on a Linux platform."""
        osName = platform.system()
        linux = False
        if osName == "Linux":
            linux = True

        return linux

    def __init__(self, uio, options):
        """@brief Constructor
           @param uio A UIO instance handling user input and output (E.G stdin/stdout or a GUI)
           @param options An instance of the OptionParser command line options."""
        super().__init__(uio, options)

    def runCmd(self, cmd):
        """@brief Run a system command.
           @param The command to execute.
           @return A tuple containing
                   0: return code
                   1: A list of stdout lines of text
                   2: A list of stderr lines of text"""
        self.debug("cmd: {}".format(cmd))
        if not isinstance(cmd, list):
            cmd = cmd.split()
        proc = Popen(cmd, stdout=PIPE, stderr=PIPE)
        out, err = proc.communicate()
        stdOutLines = out.decode().split("\n")
        stdErrLines = err.decode().split("\n")
        if self._options.debug:
            for l in stdOutLines:
                self._debug("stdout: {}".format(l))
            for l in stdErrLines:
                self._debug("stderr: {}".format(l))
        return (proc.returncode, stdOutLines, stdErrLines)

class LinuxPlatform(Platform):

    """Responsible for providing platform Linux functionality."""

    def __init__(self, uio, options):
        super().__init__(uio, options)
