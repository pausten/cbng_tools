# @brief Responsible for holding test station parameters

from p3lib.pconfig import ConfigManager

from cbng_lib.base import Base

class TestStationConfig(Base):
    #Holds generalised test parameters useful accross many test types
    LOG_PATH                    = "LOG_PATH"

    def __init__(self, uio, options, configFile, defaultConfig):
        """@brief Constructor
           @param uio A UIO instance handling user input and output (E.G stdin/stdout or a GUI)
           @param options An instance of the OptionParser command line options.
           @param configFile The name of the config file to store config to.
           @param defaultConfig A dict holding the default configuration."""
        #Call parent class constructor
        super().__init__(uio, options)
        self._configManager = ConfigManager(uio, configFile, defaultConfig)
        self._configManager.load(True)

    def show(self):
        """@brief Show the state of the test station attributes dict if debug is enabled."""
        self._uio.info('******************************')
        self._uio.info('* TEST STATION CONFIGURATION *')
        self._uio.info('******************************')
        printDict(self._uio, self.getConfigDict())

    def getConfigDict(self):
        """@brief Get the configuration dictionary."""
        return self._getTestStationConfigDict()

    def setDefaultConfig(self):
        """@Set the default test station configuration"""
        self._configManager.setDefaultConfig()

    def setAttr(self, key, value):
        """@brief Add an attribute value to the config.
           @param key The key to store the value against.
           @param value The value to be stored."""
        self._configManager.addAttr(key, value)

    def getAttr(self, key):
        """@brief Get a test attribute from the config.
           @return The attribute value."""
        return self._configManager.getAttr(key)

    def configure(self):
        """@brief Called to start the process that allows the user to set the test parameters."""
        self._configManager.configure(self.editConfig)
