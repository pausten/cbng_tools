#!/bin/sh
set -e

# Create the git hash to be included in the deb file
git rev-parse --short HEAD > assets/git_hash.txt

#Check the python files and exit on error.
pyflakes3 *.py

sudo python3 -m pipenv2deb pipenv2deb

