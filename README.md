# CBNG Tools

## Getting started
The cbng_tools package contains software tools that are initially aimed at testing
product. Therefore various programs are available once cbng_tools are installed.
Currently the ipt and iptg commands are available but in the future other tools
may be available.

## Documentation
All documentation can be found in the doc folder of the cbng_tools repo.
