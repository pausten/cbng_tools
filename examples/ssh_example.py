#!/usr/bin/env python3

# A template for CBNG command line SW tools.

import argparse
import getpass

from   cbng_lib.uio import UIO
from   cbng_lib.ssh import SSH

class CustomError(Exception):
    pass

class TestOrchestrator(object):

    def __init__(self, uio, options):
        """@brief Constructor
           @param uio A UIO instance handling user input and output (E.G stdin/stdout or a GUI)
           @param options An instance of the OptionParser command line options."""
        self._uio = uio
        self._options = options

    def execute(self):
        """@brief Execute a test orchestration."""
        ssh = None
        try:
            self._uio.info(f"Connecting to the SSH server {self._options.address} (username={self._options.username}).")
            ssh = SSH(self._options.address, self._options.username, password=self._options.password, uio=self._uio)
            self._uio.info("Connected")

        finally:
            if ssh:
                ssh.close()
                self._uio.info("Disconnected")

def main():
    """@brief Program entry point"""
    uio = UIO()
    localUsername = getpass.getuser()

    try:
        parser = argparse.ArgumentParser(description="An example of how to connect an SSH session.",
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument("-d", "--debug",   action='store_true', help="Enable debugging.")
        parser.add_argument("-a", "--address", help="The ssh server address.", default=None)
        parser.add_argument("-p", "--port",    type=int, help=f"SSH port (default={SSH.DEFAULT_SSH_PORT}).", default=SSH.DEFAULT_SSH_PORT)
        parser.add_argument("-u", "--username",help=f"The username to use when logging into the ssh server (default={localUsername})", default=localUsername)
        parser.add_argument("--password",      help="The password to use when logging into the ssh server (default=None). Can be left unset if the ssh server has a copy of the local public ssh key in it's authorized keys file.", default=None)

        options = parser.parse_args()

        uio.enableDebug(options.debug)

        testOrchestrator = TestOrchestrator(uio, options)
        testOrchestrator.execute()

    #If the program throws a system exit exception
    except SystemExit:
        pass
    #Don't print error information if CTRL C pressed
    except KeyboardInterrupt:
        pass
    except Exception as ex:

        if options.debug:
            raise
        else:
            uio.error(str(ex))

if __name__== '__main__':
    main()
