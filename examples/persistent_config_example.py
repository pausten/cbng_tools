#!/usr/bin/env python3

# When the user supplies the -c/--config argument the can change the values held
# in a local configuration file (in this case ~/.example_config.cfg).

import argparse

from   cbng_lib.uio import UIO
from   cbng_lib.test_station import TestStationConfig

class CustomError(Exception):
    pass

class PersistentConfig(TestStationConfig):
    """@brief Responsible for holding and updating the Iperf3 end to end test configuration."""

    CONFIG_FILENAME             = "example_config.cfg"
    PARAM1                      = "PARAM1"
    PARAM2                      = "PARAM2"
    PARAM3                      = "PARAM3"

    DEFAULT_CONFIG = {
        PARAM1: "STRING1",
        PARAM2: 12.45,
        PARAM3: False
    }

    def __init__(self, uio, options):
        """@brief Constructor
           @param uio A UIO instance handling user input and output (E.G stdin/stdout or a GUI)
           @param options An instance of the OptionParser command line options."""
        super().__init__(uio, options, PersistentConfig.CONFIG_FILENAME, PersistentConfig.DEFAULT_CONFIG)

    def editConfig(self, key):
        """@brief Edit a single config parameter/attribute. This is called when the
                  user wishes to edit a test parameter.
           @param key The dictionary key to edit."""

        if key == PersistentConfig.PARAM1:
            self._configManager.inputStr(PersistentConfig.PARAM1, "Enter the PARAM1 string value", False)

        if key == PersistentConfig.PARAM2:
            self._configManager.inputFloat(PersistentConfig.PARAM2, "Enter the PARAM2 float value",  minValue=-10.0, maxValue=20.0)

        if key == PersistentConfig.PARAM3:
            self._configManager.inputBool(PersistentConfig.PARAM3, "Enter the PARAM3 boolean value")

def main():
    """@brief Program entry point"""
    uio = UIO()

    try:
        parser = argparse.ArgumentParser(description="A tool to do something.\n"\
                                                     "A description of what it does.",
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument("-d", "--debug",  action='store_true', help="Enable debugging.")
        parser.add_argument("-c", "--config", action='store_true', help="Change the persistent configuration.")

        options = parser.parse_args()

        uio.enableDebug(options.debug)

        persistentConfig = PersistentConfig(uio, options)

        if options.config:
            persistentConfig.configure()

    #If the program throws a system exit exception
    except SystemExit:
        pass
    #Don't print error information if CTRL C pressed
    except KeyboardInterrupt:
        pass
    except Exception as ex:

        if options.debug:
            raise
        else:
            uio.error(str(ex))

if __name__== '__main__':
    main()
