#!/usr/bin/env python3

import argparse
import os
import time

from   pathlib import Path
from   cbng_lib.uio import UIO

from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

class CustomError(Exception):
    pass

class FolderChangeNotifier(object):
    """@brief Responsible for notifying changes to a folder."""

    def __init__(self, uio, options):
        """@brief Constructor
           @param uio A UIO instance handling user input and output (E.G stdin/stdout or a GUI)
           @param options An instance of the OptionParser command line options."""
        self._uio = uio
        self._options = options

        patterns = ["*"]
        ignore_patterns = None
        ignore_directories = False
        case_sensitive = True
        my_event_handler = PatternMatchingEventHandler(patterns, ignore_patterns, ignore_directories, case_sensitive)

        my_event_handler.on_created = self._on_created
        my_event_handler.on_deleted = self._on_deleted
        my_event_handler.on_modified = self._on_modified
        my_event_handler.on_moved = self._on_moved

        path = self._options.folder
        go_recursively = True
        self._my_observer = Observer()
        self._my_observer.schedule(my_event_handler, path, recursive=go_recursively)

    def _on_created(self, event):
        print(f"{event.src_path} has been created")

    def _on_deleted(self, event):
        print(f"{event.src_path} has been deleted")

    def _on_modified(self, event):
        print(f"{event.src_path} has been modified")

    def _on_moved(self, event):
        print(f"{event.src_path} has been moved to {event.dest_path}")

    def run(self):
        """@brief Blocking method to check for changes in the folder."""
        self._my_observer.start()
        try:
            # do something else
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            self._my_observer.stop()
            self._my_observer.join()
            raise

def main():
    """@brief Program entry point"""
    uio = UIO()

    try:
        parser = argparse.ArgumentParser(description="Example code checking for changes in the contents of a folder.",
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument("-d", "--debug",  action='store_true', help="Enable debugging.")
        parser.add_argument("-f", "--folder",   help="The folder to check for changes.", default=os.path.join(str(Path.home()), "test_logs") )

        options = parser.parse_args()

        uio.enableDebug(options.debug)
        folderChangeNotifier = FolderChangeNotifier(uio, options)
        folderChangeNotifier.run()

    #If the program throws a system exit exception
    except SystemExit:
        pass
    #Don't print error information if CTRL C pressed
    except KeyboardInterrupt:
        pass
    except Exception as ex:
        raise
        if options.debug:
            raise
        else:
            uio.error(str(ex))

if __name__== '__main__':
    main()
