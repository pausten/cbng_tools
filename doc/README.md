# CBNG Tools

## Getting started
The cbng_tools package contains software tools that are initially aimed at testing
product. Therefore various programs are available once cbng_tools are installed.
Currently the ipt and iptg commands are available but in the future other tools
may be available.

### Check what cbng tools programs are available.
Once the cbng_tools package is installed (detailed in the next section) the cbng_tools command can be used to find what commands are available, what each program does and what the command line options are available.

- Check the version of cbng_tools installed.

```
cbng_tools
INFO:  CBNG Tools GIT Hash: a63fb52
INFO:  Package: python-cbng-tools
INFO:  Status: install ok installed
INFO:  Priority: optional
INFO:  Section: Python
INFO:  Maintainer: Paul Austen <pausten@cbng.co.uk>
INFO:  Architecture: all
INFO:  Version: 1.3
INFO:  Description: Software test automation tools for use within within the CBNG organisation.
```

- Command line help

```
cbng_tools -h
INFO:  CBNG Tools GIT Hash: a63fb52
usage: cbng_tools.py [-h] [-d] [-p] [-c]

Show CBNG tools available.

options:
  -h, --help           show this help message and exit
  -d, --debug          Enable debugging.
  -p, --programs       Show all available programs.
  -c, --cmd_line_help  Show all programs and the command line help text of each.
```

- Show tools/programs available

```
cbng_tools -p
INFO:  CBNG Tools GIT Hash: a63fb52
INFO:  Package: python-cbng-tools
INFO:  Status: install ok installed
INFO:  Priority: optional
INFO:  Section: Python
INFO:  Maintainer: Paul Austen <pausten@cbng.co.uk>
INFO:  Architecture: all
INFO:  Version: 1.3
INFO:  Description: Software test automation tools for use within within the CBNG organisation.
INFO:  
INFO:  Available commands.
INFO:  
INFO:  iptg
INFO:  ipt
```

- Show command line help for each tool/program

```
cbng_tools -c
INFO:  CBNG Tools GIT Hash: 028d048
INFO:  Package: python-cbng-tools
INFO:  Status: install ok installed
INFO:  Priority: optional
INFO:  Section: Python
INFO:  Maintainer: Paul Austen <pausten@cbng.co.uk>
INFO:  Architecture: all
INFO:  Version: 1.4
INFO:  Description: Software test automation tools for use within within the CBNG organisation.
INFO:  
INFO:  CBNG tools.
INFO:  
INFO:  Command: python iptg.py -h
INFO:  INFO:  CBNG Tools GIT Hash: 028d048
INFO:  usage: iptg.py [-h] [-d] [-f FOLDER] [-o]
INFO:  
INFO:  Provide a GUI interface to the ipt tool to show the data being plotted.
INFO:  
INFO:  options:
INFO:    -h, --help            show this help message and exit
INFO:    -d, --debug           Enable debugging.
INFO:    -f FOLDER, --folder FOLDER
INFO:                          The folder to check for changes. This may also be an
INFO:                          ipt csv log file. If so then it's contents will be
INFO:                          plotted.
INFO:    -o, --overlay         Rather than separate traces overlay them on the same
INFO:                          plot area.
INFO:  
INFO:  Command: python ipt.py -h
INFO:  INFO:  CBNG Tools GIT Hash: 028d048
INFO:  usage: ipt.py [-h] [-d] [-c] [-r] [-t TX_MBPS]
INFO:  
INFO:  This tool allows the user to perform a throutput test between two endpoints using iperf3.
INFO:  The cbng_tools package must be installed on both endpoints.
INFO:  
INFO:  options:
INFO:    -h, --help            show this help message and exit
INFO:    -d, --debug           Enable debugging.
INFO:    -c, --config          Configure the persistent test parameters.
INFO:    -r, --reverse         Reverse the configured source and destination.
INFO:    -t TX_MBPS, --tx_mbps TX_MBPS
INFO:                          Override the configured TX rate in Mbps.
INFO:  
```

### Install the cbng_tools debain package.
Once the debian package has been built (see next section) run the following command from the cbng_tools folder to install a debian package file. Note that the version of the package is likely to change over time.

```
sudo dpkg -i packages/python-cbng-tools-1.0-all.deb
(Reading database ... 274943 files and directories currently installed.)
Preparing to unpack .../python-cbng-tools-1.0-all.deb ...
Unpacking python-cbng-tools (1.0) over (1.0) ...
Setting up python-cbng-tools (1.0) ...
Removing virtualenv (/usr/local/bin/python-cbng-tools.pipenvpkg/.venv)...
WARNING: --three is deprecated! pipenv uses python3 by default
Creating a virtualenv for this project...
Pipfile: /usr/local/bin/python-cbng-tools.pipenvpkg/Pipfile
Using /usr/bin/python3 (3.11.4) to create virtualenv...
⠸ Creating virtual environment...created virtual environment CPython3.11.4.final.0-64 in 181ms
  creator CPython3Posix(dest=/usr/local/bin/python-cbng-tools.pipenvpkg/.venv, clear=False, no_vcs_ignore=False, global=False)
  seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=/root/.local/share/virtualenv)
    added seed packages: pip==23.0.1, setuptools==66.1.1, wheel==0.38.4
  activators BashActivator,CShellActivator,FishActivator,NushellActivator,PowerShellActivator,PythonActivator

✔ Successfully created virtual environment!
Virtualenv location: /usr/local/bin/python-cbng-tools.pipenvpkg/.venv
Installing dependencies from Pipfile.lock (ee6a4f)...
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.
***************************************************************************
* Installation complete.                                                  *
* Run the 'cbng_tools -h' command to get details of the installed tools.  *
***************************************************************************
```

### Creating the debian package

To develop software to be added to the cbng_tools package you'll need some software
installed on your Ubuntu machine. The following details the software required.


- The python pipenv package must be isntalled.

E.G

```
sudo apt install pipenv
```

- The endpoints must have an ssh server running.

```
sudo apt install ssh
```

- pyflakes3 must be installed

E.G

```
sudo apt install pyflakes3
```

- The pipenv2deb python modules must be installed if you wish to generate debian installer packages.

E.G

```
sudo pip install --break-system-packages pipenv2deb
```

The '--break-system-packages' command line argument is not required prior to Ubuntu 23.04.


### Build the debian installer
The python code can be installed as a debian package. This debain package file can be built as shown below.

```
pja@L5540:~/git_repos/cbng_tools$ ./build.sh
INFO:  Using existing create_pip_env.sh file.
INFO:  Created build/DEBIAN
INFO:  Created build/usr/local/bin/python-cbng-tools.pipenvpkg
INFO:  Copied /home/pja/git_repos/cbng_tools/3d_parts to build/usr/local/bin/python-cbng-tools.pipenvpkg/3d_parts
INFO:  Copied /home/pja/git_repos/cbng_tools/doc to build/usr/local/bin/python-cbng-tools.pipenvpkg/doc
INFO:  Copied /home/pja/git_repos/cbng_tools/cbng_lib to build/usr/local/bin/python-cbng-tools.pipenvpkg/cbng_lib
INFO:  Copied /home/pja/git_repos/cbng_tools/examples to build/usr/local/bin/python-cbng-tools.pipenvpkg/examples
INFO:  Copied /home/pja/git_repos/cbng_tools/assets to build/usr/local/bin/python-cbng-tools.pipenvpkg/assets
INFO:  Copied Pipfile to build/usr/local/bin/python-cbng-tools.pipenvpkg
INFO:  Copied Pipfile.lock to build/usr/local/bin/python-cbng-tools.pipenvpkg
INFO:  Copied create_pip_env.sh to build/usr/local/bin/python-cbng-tools.pipenvpkg
INFO:  Copied /home/pja/git_repos/cbng_tools/iptg.py to build/usr/local/bin/python-cbng-tools.pipenvpkg
INFO:  Copied /home/pja/git_repos/cbng_tools/ipt.py to build/usr/local/bin/python-cbng-tools.pipenvpkg
INFO:  Copied /home/pja/git_repos/cbng_tools/cbng_tools.py to build/usr/local/bin/python-cbng-tools.pipenvpkg
INFO:  Creating build/DEBIAN/postinst
INFO:  Set executable attribute: build/DEBIAN/postinst
INFO:  Set executable attribute: build/DEBIAN/postinst
INFO:  Set executable attribute: build/DEBIAN/preinst
INFO:  Set executable attribute: build/DEBIAN/control
INFO:  Set executable attribute: build/usr/local/bin/python-cbng-tools.pipenvpkg/iptg.py
INFO:  Created: build/usr/local/bin/iptg
INFO:  Set executable attribute: build/usr/local/bin/iptg
INFO:  Set executable attribute: build/usr/local/bin/python-cbng-tools.pipenvpkg/ipt.py
INFO:  Created: build/usr/local/bin/ipt
INFO:  Set executable attribute: build/usr/local/bin/ipt
INFO:  Set executable attribute: build/usr/local/bin/python-cbng-tools.pipenvpkg/cbng_tools.py
INFO:  Created: build/usr/local/bin/cbng_tools
INFO:  Set executable attribute: build/usr/local/bin/cbng_tools
INFO:  Executing: dpkg-deb -Zgzip -b build packages/python-cbng-tools-1.4-all.deb
dpkg-deb: building package 'python-cbng-tools' in 'packages/python-cbng-tools-1.4-all.deb'.
INFO:  Removed build path
```


### Running CBNG tools without installing it
When developing tools you may wish to run a tool without installing it from a debian package. The following details how this may be done.
You will need the dependancies installed as detailed in the 'Creating the debian package' section.

- Checkout the cbng_tools git_repo. Until we get a company git repo you may need to request access to the cbng_repo from Paul Austen in order to clone the cbng_tools repo.

```
git clone git@github.com:pjausten/cbng_tools.git
```

- Once you have a copy of the repo locally cd to the top level folder and run the following commnd to create the pipenvpkg. This command will create a .venv folder containing all the python modules required by the cbng_tools package.

```
./create_pip_env.sh
Removing virtualenv (/home/pja/git_repos/cbng_tools/.venv)...
WARNING: --three is deprecated! pipenv uses python3 by default
Creating a virtualenv for this project...
Pipfile: /home/pja/git_repos/cbng_tools/Pipfile
Using /usr/bin/python3 (3.11.4) to create virtualenv...
⠙ Creating virtual environment...created virtual environment CPython3.11.4.final.0-64 in 132ms
  creator CPython3Posix(dest=/home/pja/git_repos/cbng_tools/.venv, clear=False, no_vcs_ignore=False, global=False)
  seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=/home/pja/.local/share/virtualenv)
    added seed packages: pip==23.0.1, setuptools==66.1.1, wheel==0.38.4
  activators BashActivator,CShellActivator,FishActivator,NushellActivator,PowerShellActivator,PythonActivator

✔ Successfully created virtual environment!
Virtualenv location: /home/pja/git_repos/cbng_tools/.venv
Installing dependencies from Pipfile.lock (540f64)...
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.
pja@L5540:~/git_repos/cbng_tools$ cat .gitignore
packagespja@L5540:~/git_repos/cbng_tools$ git status
On branch main
Your branch is up to date with 'origin/main'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   doc/README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	doc/TODO.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

- Use the pipenv python environment just Created

```
 :~/git_repos/cbng_tools$ pipenv shell
Launching subshell in virtual environment...
 :~/git_repos/cbng_tools$  . /home/pja/git
```

If you wish to develop a new tool to be added to the cbng_tools package you can copy a template from the examples folder. The main command line template file is cbng_argparse_example.py is the basic starting point for this.
