# IP Test tool
This test tool simplifies the user of iperf when you want to run between two
endpoints.

The ipt.py tool will deploy the iperf binary to the src and destination
endpoints vis ssh connections.

### Configuring the test
The ipt command line help text can be displayed as shown below.

```
ipt -h
INFO:  CBNG Tools GIT Hash: f6704ea
usage: ipt.py [-h] [-d] [-c] [-r] [-t TX_MBPS]

This tool allows the user to perform a throutput test between two endpoints using iperf3.
The cbng_tools package must be installed on both endpoints.

options:
  -h, --help            show this help message and exit
  -d, --debug           Enable debugging.
  -c, --config          Configure the persistent test parameters.
  -r, --reverse         Reverse the configured source and destination.
  -t TX_MBPS, --tx_mbps TX_MBPS
                        Override the configured TX rate in Mbps.
```

To configure the test the following command can be used.

```
ipt -c
INFO:  CBNG Tools GIT Hash: 028d048
INFO:  Loaded config from /home/pja/.ipt.cfg
INFO:  ID  PARAMETER                VALUE
INFO:  1   TEST_TYPE                UDP
INFO:  2   SRC_DATA_IF_ADDRESS      192.168.0.9
INFO:  3   SRC_SSH_SERVER_ADDRESS   192.168.0.9
INFO:  4   SRC_SSH_PORT             22
INFO:  5   SRC_SSH_USERNAME         pja
INFO:  6   SRC_TX_MBPS              1000.0
INFO:  7   DEST_DATA_IF_ADDRESS     192.168.0.8
INFO:  8   DEST_SSH_SERVER_ADDRESS  192.168.0.8
INFO:  9   DEST_SSH_PORT            22
INFO:  10  DEST_SSH_USERNAME        pja
INFO:  11  CONNECT_TIMEOUT          5.0
INFO:  12  STREAM_COUNT             1
INFO:  13  BI_DIR                   True
INFO:  14  TYPE_OF_SERVICE_FIELD    0
INFO:  15  DIFF_SRV_FIELD           0
INFO:  16  TCP_WINDOW_SIZE          0
INFO:  17  TCP_MSS_SIZE             0
INFO:  18  STARTUP_SECONDS          1
INFO:  19  RX_TIMEOUT_SECS          3
INFO:  20  DONT_FRAGMENT            True
INFO:  21  UPDATE_PERIOD            1.0
INFO:  22  TEST_PERIOD              20.0
INFO:  23  UDP_FRAME_SIZE           1518
INFO:  24  UDP_802_1Q_ENABLED       False
INFO:  25  UDP_TEST_RATE_LAYER      1
INFO:  26  LOG_PATH                 /home/pja/test_logs
INPUT: Enter 'E' to edit a parameter, or 'Q' to quit:
```

The above example runs a test from your local machine to your local machine.
Not very useful but for an example a usful place to start.

#PJA TODO add documentation detailing what each of the above parameters do.

### Running a test
Before running a test the src and destination machines must have the local
(machine where cbng_tools is checked out) in their authorised keys file.

To do this use the ssh-copy-id command.

The example above uses an src and destination endpoint as localhost. The
example below shows this public key being added.

```
ssh-copy-id localhost
The authenticity of host 'localhost (127.0.0.1)' can't be established.
ED25519 key fingerprint is SHA256:+fGy5Q3O/BsqCmodhfqmYP5DfYJ+0ALDem11Z0HyDkY.
This host key is known by the following other names/addresses:
    ~/.ssh/known_hosts:6: [hashed name]
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
pja@localhost's password:

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'localhost'"
and check to make sure that only the key(s) you wanted were added.
```

Once this is done the test can be started as shown below.

```
ipt
INFO:  CBNG Tools GIT Hash: f6704ea
INFO:  Loaded config from /home/pja/.ipt.cfg
INFO:  Protocol:            TCP
INFO:  Direction:           Bi
INFO:  Source:      localhost (ssh server localhost:22)
INFO:  Destination: localhost (ssh server localhost:22)
INFO:  Attempting to connect to the source ssh server on localhost:22
INFO:  Connected
INFO:  Attempting to connect to the destination ssh server on localhost:22
INFO:  Connected
INFO:  -------------------------------------------------------------------------------------------------
INFO:  |         | Src TX | Src TX  | Src TX      | Src RX | Dest TX | Dest TX | Dest TX     | Dest RX |
INFO:  | Seconds | Mbps   | Retries | CWnd KBytes | Mbps   | Mbps    | Retries | CWnd KBytes | Mbps    |
INFO:  -------------------------------------------------------------------------------------------------
INFO:  | 1       | 27318.0 | 0       | 3           | 26668.0 | 27      | 4       | 3           | 27.3    |
INFO:  | 2       | 26017.0 | 0       | 3           | 24084.0 | 24      | 3       | 3           | 26.2    |
INFO:  | 3       | 23415.0 | 0       | 3           | 23943.0 | 24      | 0       | 3           | 23.4    |
INFO:  | 4       | 25129.0 | 0       | 3           | 24131.0 | 24      | 0       | 3           | 25.3    |
INFO:  | 5       | 25422.0 | 0       | 3           | 26971.0 | 27      | 3       | 3           | 25.4    |
INFO:  | 6       | 24243.0 | 0       | 3           | 25852.0 | 25      | 0       | 3           | 24.3    |
INFO:  | 7       | 27036.0 | 0       | 3           | 26983.0 | 27      | 0       | 3           | 27.0    |
INFO:  | 8       | 26281.0 | 0       | 3           | 26131.0 | 26      | 0       | 3           | 26.2    |
INFO:  | 9       | 27173.0 | 0       | 3           | 25966.0 | 26      | 0       | 3           | 27.2    |
INFO:  | 10      | 25440.0 | 0       | 3           | 25906.0 | 26      | 0       | 3           | 25.5    |
INFO:  | 11      | 26817.0 | 0       | 3           | 26214.0 | 26      | 0       | 3           | 26.8    |
INFO:  | 12      | 24197.0 | 0       | 3           | 25442.0 | 25      | 0       | 3           | 24.2    |
INFO:  | 13      | 25492.0 | 0       | 3           | 24457.0 | 24      | 0       | 3           | 25.7    |
INFO:  | 14      | 25021.0 | 0       | 3           | 25001.0 | 25      | 0       | 3           | 25.0    |
INFO:  | 15      | 27252.0 | 0       | 3           | 27566.0 | 28      | 0       | 3           | 27.3    |
INFO:  | 16      | 26447.0 | 0       | 3           | 27751.0 | 28      | 0       | 3           | 26.5    |
INFO:  | 17      | 26338.0 | 0       | 3           | 27546.0 | 28      | 0       | 3           | 27.4    |
INFO:  | 18      | 26614.0 | 0       | 3           | 27371.0 | 27      | 0       | 3           | 26.6    |
INFO:  | 19      | 26767.0 | 0       | 3           | 26762.0 | 27      | 0       | 3           | 26.8    |
INFO:  | 20      | 26662.0 | 0       | 3           | 27427.0 | 27      | 0       | 3           | 26.7    |
INFO:  | 21      | 27190.0 | 0       | 3           | 27163.0 | 27      | 0       | 3           | 27.2    |
INFO:  | 22      | 26329.0 | 0       | 3           | 27718.0 | 28      | 0       | 3           | 26.4    |
INFO:  | 23      | 26827.0 | 0       | 3           | 27261.0 | 27      | 0       | 3           | 26.8    |
INFO:  | 24      | 26277.0 | 0       | 3           | 25842.0 | 26      | 0       | 3           | 26.3    |
INFO:  | 25      | 26996.0 | 0       | 3           | 25293.0 | 25      | 0       | 3           | 27.0    |
INFO:  | 26      | 25595.0 | 0       | 3           | 24413.0 | 24      | 0       | 3           | 26.1    |
INFO:  | 27      | 27059.0 | 0       | 3           | 26969.0 | 27      | 0       | 3           | 27.1    |
INFO:  | 28      | 25187.0 | 0       | 3           | 26176.0 | 26      | 0       | 3           | 25.4    |
INFO:  | 29      | 26840.0 | 0       | 3           | 26886.0 | 27      | 0       | 3           | 26.9    |
INFO:  | 30      | 25755.0 | 0       | 3           | 25873.0 | 26      | 1       | 3           | 25.8    |
INFO:  | 31      | 27659.0 | 0       | 3           | 27215.0 | 27      | 1       | 3           | 27.6    |
INFO:  | 32      | 26596.0 | 0       | 3           | 26234.0 | 26      | 14      | 3           | 26.6    |
INFO:  | 33      | 27252.0 | 0       | 3           | 26930.0 | 27      | 0       | 3           | 27.3    |
INFO:  | 34      | 26324.0 | 0       | 3           | 25006.0 | 25      | 4       | 3           | 26.3    |
INFO:  | 35      | 27339.0 | 0       | 3           | 25679.0 | 25      | 0       | 3           | 27.3    |
INFO:  | 36      | 26366.0 | 0       | 3           | 26135.0 | 26      | 0       | 3           | 26.4    |
INFO:  | 37      | 27104.0 | 0       | 3           | 26458.0 | 26      | 0       | 3           | 27.1    |
INFO:  | 38      | 25686.0 | 0       | 3           | 27276.0 | 27      | 0       | 3           | 25.8    |
INFO:  | 39      | 23956.0 | 0       | 3           | 24306.0 | 24      | 0       | 3           | 24.0    |
INFO:  | 40      | 26558.0 | 0       | 3           | 25956.0 | 26      | 0       | 3           | 26.6    |
INFO:  | 41      | 24845.0 | 0       | 3           | 24833.0 | 25      | 0       | 3           | 24.8    |
INFO:  | 42      | 23870.0 | 0       | 3           | 24250.0 | 24      | 0       | 3           | 23.9    |
INFO:  | 43      | 27412.0 | 0       | 3           | 26544.0 | 26      | 0       | 3           | 27.4    |
INFO:  | 44      | 26923.0 | 0       | 3           | 26071.0 | 26      | 0       | 3           | 26.9    |
INFO:  | 45      | 26249.0 | 0       | 3           | 26469.0 | 26      | 0       | 3           | 26.2    |
INFO:  | 46      | 26400.0 | 0       | 3           | 25744.0 | 26      | 0       | 3           | 26.4    |
INFO:  | 47      | 26164.0 | 0       | 3           | 26425.0 | 26      | 0       | 3           | 26.2    |
INFO:  | 48      | 26922.0 | 0       | 3           | 26748.0 | 27      | 0       | 3           | 26.9    |
INFO:  | 49      | 26964.0 | 0       | 3           | 27516.0 | 28      | 0       | 3           | 27.2    |
INFO:  | 50      | 26137.0 | 0       | 3           | 27424.0 | 27      | 0       | 3           | 26.3    |
INFO:  | 51      | 27016.0 | 0       | 3           | 27530.0 | 28      | 0       | 3           | 27.2    |
INFO:  | 52      | 26625.0 | 0       | 3           | 27258.0 | 27      | 0       | 3           | 26.6    |
INFO:  | 53      | 27081.0 | 0       | 3           | 27348.0 | 27      | 0       | 3           | 27.1    |
INFO:  | 54      | 26852.0 | 0       | 3           | 26140.0 | 26      | 0       | 3           | 26.9    |
INFO:  | 55      | 27225.0 | 0       | 3           | 27637.0 | 28      | 0       | 3           | 27.2    |
INFO:  | 56      | 26102.0 | 0       | 3           | 27242.0 | 27      | 0       | 3           | 26.1    |
INFO:  | 57      | 27072.0 | 0       | 3           | 27070.0 | 27      | 0       | 3           | 27.1    |
INFO:  | 58      | 25381.0 | 0       | 3           | 23924.0 | 24      | 0       | 3           | 25.4    |
INFO:  | 59      | 26421.0 | 0       | 3           | 27173.0 | 27      | 2       | 3           | 26.4    |
INFO:  | 60      | 25710.0 | 0       | 3           | 26451.0 | 26      | 0       | 3           | 25.7    |
INFO:  -------------------------------------------------------------------------------------------------
INFO:  | Result  | 26315.0 | 0       |             | 26282.0 | 26283   | 32      |             | 26314.0 |
INFO:  -------------------------------------------------------------------------------------------------
```
