#!/usr/bin/env python3

import argparse
import sys
import os

from   time import sleep

from   cbng_lib.uio import UIO
from   cbng_lib.test_station import TestStationConfig
from   cbng_lib.iperf3_if import IP3PPConfig, IP3Manager, IP3Output
from   cbng_lib.base import Base

class IPerf3TestError(Exception):
    pass

class EETConfig(TestStationConfig):
    """@brief Responsible for holding and updating the Iperf3 end to end test configuration."""

    CONFIG_FILENAME             = "ipt.cfg"
    TEST_TYPE                   = "TEST_TYPE"
    SRC_SSH_SERVER_ADDRESS      = "SRC_SSH_SERVER_ADDRESS"
    SRC_SSH_PORT                = "SRC_SSH_PORT"
    SRC_SSH_USERNAME            = "SRC_SSH_USERNAME"
    SRC_DATA_IF_ADDRESS         = "SRC_DATA_IF_ADDRESS"
    SRC_TX_MBPS                 = "SRC_TX_MBPS"
    DEST_SSH_SERVER_ADDRESS     = "DEST_SSH_SERVER_ADDRESS"
    DEST_SSH_PORT               = "DEST_SSH_PORT"
    DEST_SSH_USERNAME           = "DEST_SSH_USERNAME"
    DEST_DATA_IF_ADDRESS        = "DEST_DATA_IF_ADDRESS"
    CONNECT_TIMEOUT             = "CONNECT_TIMEOUT"
    STREAM_COUNT                = "STREAM_COUNT"
    BI_DIR                      = "BI_DIR"
    TYPE_OF_SERVICE_FIELD       = "TYPE_OF_SERVICE_FIELD"
    DIFF_SRV_FIELD              = "DIFF_SRV_FIELD"
    TCP_WINDOW_SIZE             = "TCP_WINDOW_SIZE"
    TCP_MSS_SIZE                = "TCP_MSS_SIZE"
    STARTUP_SECONDS             = "STARTUP_SECONDS"
    RX_TIMEOUT_SECS             = "RX_TIMEOUT_SECS"
    DONT_FRAGMENT               = "DONT_FRAGMENT"
    UPDATE_PERIOD               = "UPDATE_PERIOD"
    TEST_PERIOD                 = "TEST_PERIOD"
    UDP_FRAME_SIZE              = "UDP_FRAME_SIZE"
    UDP_802_1Q_ENABLED          = "UDP_802_1Q_ENABLED"
    UDP_TEST_RATE_LAYER         = "UDP_TEST_RATE_LAYER"
    
    DEFAULT_CONFIG = {
        TEST_TYPE:                "",       # Either UDP or TCP
        SRC_DATA_IF_ADDRESS:      "",       # The address of the src endpoint data interface.
        SRC_SSH_SERVER_ADDRESS:   "",       # The address of the src endpoint control interface (may be the same as the data IF address).
        SRC_SSH_PORT:             22,       # The ssh port to connect to the src endpoint
        SRC_SSH_USERNAME:         "",       # The ssh username for the src end of the connection.
        SRC_TX_MBPS:              0.0,      # The TX rate from the SRC endpoint.
        DEST_DATA_IF_ADDRESS:     "",       # The address of the dest endpoint data interface.
        DEST_SSH_SERVER_ADDRESS:  "",       # The address of the dest endpoint control interface (may be the same as the data IF address).
        DEST_SSH_PORT:             22,      # The ssh port to connect to the dest endpoint
        DEST_SSH_USERNAME:         "",      # The ssh username for the dest end of the connection.
        CONNECT_TIMEOUT:          5.0,      # The max time allowed to connect between the src and dest data IF addresses.
        STREAM_COUNT:             1,        # The number of seprate streams to run.
        BI_DIR:                   False,    # Wether the test is uni or Bi directional. True if bi directional.
        TYPE_OF_SERVICE_FIELD:    0,        # The value in the ype of service field (TOS) in the packets sent from the src.
        DIFF_SRV_FIELD:           0,        # The value in the differentiated servcies field (DIFFSRV) in the packets sent from the src.
        TCP_WINDOW_SIZE:          0,        # The TCP window size for the test stream if running a TCP test.
        TCP_MSS_SIZE:             0,        # The TCP MSS when running a TCP test.
        STARTUP_SECONDS:          1,        # The number of seconds to elapse before we start measuring data throughput. Important for TCP
                                            # tests as the throughput climbs as the TCP algorithm moves to the max achievable window size.
        RX_TIMEOUT_SECS:          3,        # The number of seconds with no data receivedd before we consider the test as stopped.
        DONT_FRAGMENT:            True,     # If True then don't fragment packets as they are sent.
        UPDATE_PERIOD:            1.0,      # The update period in seconds for data sent and received
        TEST_PERIOD:              60.0,     # The time for the test to run in seconds.
        UDP_FRAME_SIZE:           1518,     # The default Ethernet frame size during a UDP test.
        UDP_802_1Q_ENABLED:       False,    # If 802.1Q is enabled an extra 4 bytes are present in the Ethernet frame.
                                            # If enabled and the test is a UDP test then this is taken into account when
                                            # calculating the UDP payload size.
        UDP_TEST_RATE_LAYER:      2,        # The layer at which the UDP TX and RX rates are displayed. Default is L2 (Ethernet frame)
                                            # Other options are 
                                            # 1 = L1 Mbps (includes the Ethernet interframe gap and preamble)
                                            # 3 = UDP payload Mbps. 
#        PJA TODO ADD 802.1Q enabled
        TestStationConfig.LOG_PATH:         "{}/test_logs".format( Base.GetHomePath() )
    }
    
    def __init__(self, uio, options):
        """@brief Constructor
           @param uio A UIO instance handling user input and output (E.G stdin/stdout or a GUI)
           @param options An instance of the OptionParser command line options."""
        super().__init__(uio, options, EETConfig.CONFIG_FILENAME, EETConfig.DEFAULT_CONFIG)

    def _enterTestType(self):
        """@brief Allow the user to enter the type of test required."""
        while True:
            self._configManager.inputStr(EETConfig.TEST_TYPE, "Enter the test type (udp or tcp)", False)
            response = self._configManager.getAttr(EETConfig.TEST_TYPE)
            if response.lower() == 'udp':
                # Force upper case
                response = self._configManager.addAttr(EETConfig.TEST_TYPE, response.upper())
                break
            elif response.lower() == 'tcp':
                # Force upper case
                response = self._configManager.addAttr(EETConfig.TEST_TYPE, response.upper())
                break
            else:
                self.error("Enter either UDP or TCP as the test type.")

    def _enterString(self, parameter, prompt):
        """@brief Allow the user to enter any non zero length string.
           @param parameter The test parameter to be changed.
           @param prompt The user prompt."""
        while True:
            self._configManager.inputStr(parameter, prompt, False)
            response = self._configManager.getAttr(parameter)
            if len(response) > 0:
                break
            else:
                self.error("You must enter a value for this parameter.")

    def _showCommonEPAMessage(self):
        """@brief Show a message that is common accross all endpoint address entries."""
        self.info("If an endpoint has only one interface then the data and ssh addresses will be the same.")

    def _enterEPTXRate(self, parameter, prompt):
        """@brief Allow the user to enter any non zero length string.
           @param parameter The test parameter to be changed.
           @param prompt The user prompt."""
        while True:
            self._configManager.inputFloat(parameter, prompt, False)
            response = self._configManager.getAttr(parameter)
            if response >= 10:
                break
            else:
                self.error("You must enter a value that is 10 or greater for this parameter.")

    def editConfig(self, key):
        """@brief Edit a single config parameter/attribute.
           @param key The dictionary key to edit."""

        if key == EETConfig.TEST_TYPE:
            self._enterTestType()

        elif key == EETConfig.SRC_SSH_SERVER_ADDRESS:
            self._showCommonEPAMessage()
            self._enterString(EETConfig.SRC_SSH_SERVER_ADDRESS, "Enter the source control interface address ")

        elif key == EETConfig.SRC_DATA_IF_ADDRESS:
            self._showCommonEPAMessage()
            self._enterString(EETConfig.SRC_DATA_IF_ADDRESS, "Enter the source data interface address ")

        elif key == EETConfig.SRC_TX_MBPS:
            self._enterEPTXRate(EETConfig.SRC_TX_MBPS, "Enter the source TX rate in Mbps")

        elif key == EETConfig.DEST_SSH_SERVER_ADDRESS:
            self._showCommonEPAMessage()
            self._enterString(EETConfig.DEST_SSH_SERVER_ADDRESS, "Enter destination control interface address")

        elif key == EETConfig.DEST_DATA_IF_ADDRESS:
            self._showCommonEPAMessage()
            self._enterString(EETConfig.DEST_DATA_IF_ADDRESS, "Enter the destination data interface address")

        elif key == EETConfig.CONNECT_TIMEOUT:
            self._configManager.inputFloat(EETConfig.CONNECT_TIMEOUT, "Enter the connection timeout in seconds")

        elif key == EETConfig.STREAM_COUNT:
            self._configManager.inputDecInt(EETConfig.STREAM_COUNT, "Enter the connection timeout in seconds", minValue=1, maxValue=10)

        elif key == EETConfig.BI_DIR:
            self._configManager.inputBool(EETConfig.BI_DIR, "Bi directional test")

        elif key == EETConfig.TYPE_OF_SERVICE_FIELD:
            self._configManager.inputDecInt(EETConfig.TYPE_OF_SERVICE_FIELD, "Enter the type of service (TOS) field for the packets sent (0 - 255)", minValue=0, maxValue=255)

        elif key == EETConfig.TYPE_OF_SERVICE_FIELD:
            self._configManager.inputDecInt(EETConfig.TYPE_OF_SERVICE_FIELD, "Enter the type of service (TOS) field for the packets sent (0 - 255)", minValue=0, maxValue=255)

        elif key == EETConfig.DIFF_SRV_FIELD:
            self._configManager.inputDecInt(EETConfig.DIFF_SRV_FIELD, "Enter the differentiated services (DiffServ) field for the packets sent (0 - 63)", minValue=0, maxValue=63)

        elif key == EETConfig.TCP_WINDOW_SIZE:
            self._uio.warn("!!! Changing this from the default system setting may reduce the test throughput !!!")
            self._uio.info("For TCP tests this sets the TCP window size. Set to 0 to use the system setting.")
            self._uio.info("For UDP tests this sets the receive buffer size. Set to 0 to use the system setting.")
            self._configManager.inputDecInt(EETConfig.TCP_WINDOW_SIZE, "Enter the window size in bytes to use when testing", minValue=0, maxValue=425000)

        elif key == EETConfig.TCP_MSS_SIZE:
            self._uio.warn("!!! Changing this from the default system setting may reduce the test throughput !!!")
            self._uio.info("Set the TCP MSS size to 0 to use the system setting.")
            self._uio.info("A typical value for the MSS would be 1460 bytes with 1500 MTU size on an Ethernet interface.")
            self._configManager.inputDecInt(EETConfig.TCP_MSS_SIZE, "Enter the TCP MSS size to use when testing", minValue=0, maxValue=9216)

        elif key == EETConfig.STARTUP_SECONDS:
            self._configManager.inputDecInt(EETConfig.STARTUP_SECONDS, "Enter the time to wait for the test to startup in seconds", minValue=0, maxValue=60)

        elif key == EETConfig.RX_TIMEOUT_SECS:
            self._configManager.inputFloat(EETConfig.RX_TIMEOUT_SECS, "Enter the RX timeout field for test data", minValue=1.0, maxValue=600.0)

        elif key == EETConfig.UDP_FRAME_SIZE:
            self._uio.warn("!!! This parameter is only valid for UDP tests.")
            self._uio.info("The default Ethernet frame size is 1518 bytes (L2 including FCS/CRC).")
            self._uio.info("If a value over 1518 is entered then the interface must be configured for jumbo frames.")
            self._configManager.inputDecInt(EETConfig.UDP_FRAME_SIZE, "Enter the required Ethernet frame size", minValue=64, maxValue=9000)
            
        elif key == EETConfig.UDP_802_1Q_ENABLED:
            self._uio.warn("!!! This parameter is only valid for UDP tests.")
            self._uio.info("If the Linux interfaces are configured to 802.1Q enable this option to include")
            self._uio.info("the 4 byte 801.1Q in the Ethernet frame size.")
            self._configManager.inputBool(EETConfig.UDP_802_1Q_ENABLED, "801.1Q enabled on Ethernet interface.")
            
        elif key == EETConfig.UDP_TEST_RATE_LAYER:
            self._uio.warn("!!! This parameter is only valid for UDP tests.")
            self._uio.info("The options are")
            self._uio.info("1 = Layer 1 rate. This includes the Ethernet interframe gap and preamble.")
            self._uio.info("2 = Layer 2/Ethernet frame rate.")
            self._uio.info("3 = Layer 3/UDP payload rate.")
            self._configManager.inputDecInt(EETConfig.UDP_TEST_RATE_LAYER, "Enter the UDP test TX/RX rate layer", minValue=0, maxValue=60)
            
        elif key == EETConfig.DONT_FRAGMENT:
            self._configManager.inputBool(EETConfig.DONT_FRAGMENT, "Set the IPV4 don't fragment flag")

        elif key == EETConfig.SRC_SSH_USERNAME:
            self._enterString(EETConfig.SRC_SSH_USERNAME, "Enter the src ssh server username")

        elif key == EETConfig.DEST_SSH_USERNAME:
            self._enterString(EETConfig.DEST_SSH_USERNAME, "Enter the dest ssh server username")

        elif key == EETConfig.UPDATE_PERIOD:
            self._configManager.inputFloat(EETConfig.UPDATE_PERIOD, "Enter the poll time for stats in seconds", minValue=1.0, maxValue=30)

        elif key == EETConfig.TEST_PERIOD:
            self._configManager.inputFloat(EETConfig.TEST_PERIOD, "Enter the time that the test should run for in seconds", minValue=0.0, maxValue=80000)

    def getIP3PPConfig(self, reverse=False):
        """@brief Get an IP3PPConfig instance with the attributes of the configuration entered.
           @param reverse If True then the source and destination are reversed.
           @return A IP3PPConfig instance with the attributes set as per this configuration instance."""
        ip3PPConfig = IP3PPConfig()

        if reverse:
            ip3PPConfig.destSSHHost = self._configManager.getAttr(EETConfig.SRC_SSH_SERVER_ADDRESS)
            ip3PPConfig.destSSHPort = self._configManager.getAttr(EETConfig.SRC_SSH_PORT)
            ip3PPConfig.destSSHUsername = self._configManager.getAttr(EETConfig.SRC_SSH_USERNAME)
            ip3PPConfig.destTestIFAddress = self._configManager.getAttr(EETConfig.SRC_DATA_IF_ADDRESS)

            ip3PPConfig.srcSSHHost = self._configManager.getAttr(EETConfig.DEST_SSH_SERVER_ADDRESS)
            ip3PPConfig.srcSSHPort = self._configManager.getAttr(EETConfig.DEST_SSH_PORT)
            ip3PPConfig.srcSSHUsername = self._configManager.getAttr(EETConfig.DEST_SSH_USERNAME)
            ip3PPConfig.srcTestIFAddress = self._configManager.getAttr(EETConfig.DEST_DATA_IF_ADDRESS)

        else:
            ip3PPConfig.srcSSHHost = self._configManager.getAttr(EETConfig.SRC_SSH_SERVER_ADDRESS)
            ip3PPConfig.srcSSHPort = self._configManager.getAttr(EETConfig.SRC_SSH_PORT)
            ip3PPConfig.srcSSHUsername = self._configManager.getAttr(EETConfig.SRC_SSH_USERNAME)
            ip3PPConfig.srcTestIFAddress = self._configManager.getAttr(EETConfig.SRC_DATA_IF_ADDRESS)

            ip3PPConfig.destSSHHost = self._configManager.getAttr(EETConfig.DEST_SSH_SERVER_ADDRESS)
            ip3PPConfig.destSSHPort = self._configManager.getAttr(EETConfig.DEST_SSH_PORT)
            ip3PPConfig.destSSHUsername = self._configManager.getAttr(EETConfig.DEST_SSH_USERNAME)
            ip3PPConfig.destTestIFAddress = self._configManager.getAttr(EETConfig.DEST_DATA_IF_ADDRESS)

        ip3PPConfig.pollSeconds = self._configManager.getAttr(EETConfig.UPDATE_PERIOD)
        ip3PPConfig.testSeconds = self._configManager.getAttr(EETConfig.TEST_PERIOD)
        if self._options.tx_mbps >= 0:
            ip3PPConfig.mbps = self._options.tx_mbps
        else:
            ip3PPConfig.mbps = self._configManager.getAttr(EETConfig.SRC_TX_MBPS)
           
        protocol = self._configManager.getAttr(EETConfig.TEST_TYPE)
        protocol=protocol.lower()
        if protocol == 'udp':
            ip3PPConfig.udp = True
        else:
            ip3PPConfig.udp = False
        ip3PPConfig.tos = self._configManager.getAttr(EETConfig.TYPE_OF_SERVICE_FIELD)
        ip3PPConfig.diffSrv = self._configManager.getAttr(EETConfig.DIFF_SRV_FIELD)
        ip3PPConfig.connectTimeout = self._configManager.getAttr(EETConfig.CONNECT_TIMEOUT)*1000
        ip3PPConfig.streamCount = self._configManager.getAttr(EETConfig.STREAM_COUNT)
        ip3PPConfig.bidir = self._configManager.getAttr(EETConfig.BI_DIR)
        ip3PPConfig.window = self._configManager.getAttr(EETConfig.TCP_WINDOW_SIZE)
        ip3PPConfig.mss = self._configManager.getAttr(EETConfig.TCP_MSS_SIZE)
        ip3PPConfig.startupSeconds = self._configManager.getAttr(EETConfig.STARTUP_SECONDS)
        
        if protocol == 'udp':
            udpRateLayer = self._configManager.getAttr(EETConfig.UDP_TEST_RATE_LAYER)
            ethFrameSize = self._configManager.getAttr(EETConfig.UDP_FRAME_SIZE)
            iee8021QEbabled = self._configManager.getAttr(EETConfig.UDP_802_1Q_ENABLED)
            ip3PPConfig.updateTXRate(udpRateLayer, ethFrameSize, iee8021QEbabled)
        
        return ip3PPConfig

class EETTest(Base):

    def __init__(self, uio, options):
        """@brief Constructor
           @param uio A UIO instance handling user input and output (E.G stdin/stdout or a GUI)
           @param options An instance of the OptionParser command line options."""
        super().__init__(uio, options)

    def _setLogFilename(self, end2EndTestConfig):
        """@brief Set the name of the UIO log file. The log file of all the output the users sees.
                  A file of the same name with the additional .debug.txt suffix will also be created
                  which holds all debug data.
           @param end2EndTestConfig An EETConfig instance.
           @return The log file."""
        logPath = end2EndTestConfig.getAttr(EETConfig.LOG_PATH)
        file = sys.argv[0]
        # Remove the path from the file
        file = os.path.basename(file)
        # Remove the .py from the log file name
        file = file.replace(".py", "")
        logFile = Base.GetLogFile(logPath, file)
        self._uio.setLogFile( logFile )
        self._uio.logAll(True)
        return logFile

    def runTest(self, end2EndTestConfig):
        """@brief Perform a throughput test.
           @param end2EndTestConfig An EETConfig instance."""
        ip3Manager = None
        try:
            showColor = True

            self._setLogFilename(end2EndTestConfig)

            # Set some flags used when displaying the test data
            biDir = end2EndTestConfig.getAttr(EETConfig.BI_DIR)
            udp = False
            protocol = end2EndTestConfig.getAttr(EETConfig.TEST_TYPE)
            if protocol == "UDP":
                udp = True
            streamCount = end2EndTestConfig.getAttr(EETConfig.STREAM_COUNT)

            if udp:
                self._uio.info("Protocol:            UDP")
            else:
                self._uio.info("Protocol:            TCP")

            if biDir:
                self._uio.info("Direction:           Bi")
            else:
                self._uio.info("Direction:           Uni")

            ip3PPConfig = end2EndTestConfig.getIP3PPConfig(reverse=self._options.reverse)
            self._uio.info("Source:      {} (ssh server {}:{})".format(ip3PPConfig.srcTestIFAddress,\
                                                                         ip3PPConfig.srcSSHHost,\
                                                                         ip3PPConfig.srcSSHPort))
            self._uio.info("Destination: {} (ssh server {}:{})".format(ip3PPConfig.destTestIFAddress,\
                                                                         ip3PPConfig.destSSHHost,\
                                                                         ip3PPConfig.destSSHPort))
            # Start the test
            ip3Manager = IP3Manager(ip3PPConfig, uio=self._uio)
            ip3Manager.start()

            ip3Output = IP3Output(self._uio, biDir, udp, streamCount, showColor=showColor, rateFactor=ip3PPConfig.rateFactor)

            ip3Output.showTableHeader()

            testRunning = True
            while testRunning:
                # This method should not block getting the test data
                iperfDictList = ip3Manager.getIPerfDictList()
                # If we have some stats
                if iperfDictList:
                    if ip3Manager.IsTestFinished(iperfDictList):
                        testRunning = False
                    else:
                        ip3Output.showProgress(iperfDictList)

                # Don't spin to fast consuming CPU cycles
                sleep(0.05)

        finally:
            if ip3Manager:
                ip3Manager.shutDown()
                ip3Manager = None


def main():
    """@brief Program entry point"""
    uio = UIO()

    try:
        parser = argparse.ArgumentParser(description="This tool allows the user to perform a throutput test between two endpoints using iperf3.\n"\
                                                     "The cbng_tools package must be installed on both endpoints.",
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument("-d", "--debug",   action='store_true', help="Enable debugging.")
        parser.add_argument("-c", "--config",  action='store_true', help="Configure the persistent test parameters.")
        parser.add_argument("-r", "--reverse", action='store_true', help="Reverse the configured source and destination.")
        parser.add_argument("-t", "--tx_mbps", type=float, help="Override the configured TX rate in Mbps.", default=-1.0)

        options = parser.parse_args()

        uio.enableDebug(options.debug)
        end2EndTest = EETTest(uio, options)
        end2EndTestConfig = EETConfig(uio, options)

        if options.config:
            end2EndTestConfig.configure()

        else:
            end2EndTest.runTest(end2EndTestConfig)

    #If the program throws a system exit exception
    except SystemExit:
        pass
    #Don't print error information if CTRL C pressed
    except KeyboardInterrupt:
        pass
    except Exception as ex:

        if options.debug:
            raise
        else:
            uio.error(str(ex))

if __name__== '__main__':
    main()
