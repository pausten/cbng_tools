#!/usr/bin/env python3

import argparse
import os

from time import sleep

from   pathlib import Path
from   cbng_lib.uio import UIO
from   cbng_lib.base import Base

from   watchdog.observers import Observer
from   watchdog.events import PatternMatchingEventHandler

from    p3lib.table_plot import Table2DPlotServer
from    p3lib.json_networking import JSONClient
from    p3lib.bokeh_gui import SingleAppServer

class GUILauncher(Base):
    """@brief Responsible for launching the GUI in a web browser window."""

    def __init__(self, uio, options):
        """@brief Constructor
           @param uio A UIO instance handling user input and output (E.G stdin/stdout or a GUI)
           @param options An instance of the OptionParser command line options."""
        self._uio = uio
        self._options = options

    def _parseColNameList(self, tableColNames):
        """@brief Parse the column name list."""
        newColNames = []
        for colName in tableColNames:
            newColNames.append( colName.rstrip("\n") )
        return newColNames

    def initGUI(self, tableColNames):
        """@brief Called to open a new GUI window with separate plots for each column."""
        tableColNames = self._parseColNameList(tableColNames)
        self._openWindow(tableColNames)

    def _parseResultSet(self, resultSet):
        """@brief Parse a results set. A result set is a single value for all columns in a table."""
        colValues = []
        if len(resultSet) > 0:
            # If this is the final (agregate) result.
            if resultSet[0].strip() == 'Result':
                print("Test complete")
                for colValue in resultSet:
                    colValues.append( colValue.strip() )
            else:
                for colValue in resultSet:
                    colValues.append( colValue.strip() )
        return colValues

    def _checkForError(self):
        return
        rxDict = self._client.rx(blocking=False)
        if rxDict and Table2DPlotServer.ERROR in rxDict:
            error = rxDict[Table2DPlotServer.ERROR]
            raise Exception(error)


    def addResultSet(self, resultSet):
        """@brief Called to open a new GUI window with separate plots for each column."""
        resultSet = self._parseResultSet(resultSet)
        if len(resultSet) > 0:
            if resultSet[0] == 'Result':
                rowDict = {Table2DPlotServer.SET_RESULT: resultSet[1:]}
                self._client.tx(rowDict)
            else:
                resultSet = [float(i) for i in resultSet]
                rowDict = {Table2DPlotServer.TABLE_ROW: resultSet}
                self._client.tx(rowDict)
        self._checkForError()

    def _openWindow(self, tableColNames):
        """@brief Open a new window in the web browser.
           @param tableColNames The names of the table columns to be plotted."""
        port = SingleAppServer.GetNextUnusedPort()
        # This starts the server in the background waiting for the data to be plotted.
        table2DPlotServer = Table2DPlotServer()
        table2DPlotServer.setPort(port)
        table2DPlotServer.start()

        # Create the client to talk to the above server and send parameters and table data
        self._client = JSONClient(Table2DPlotServer.DEFAULT_HOST, port)
        # Set the X Axis type
        # Set X AXIS as a float value values/seconds
        xAxisTypeDict = {Table2DPlotServer.XAXIS_TYPE: Table2DPlotServer.FLOAT_X_AXIS_TYPE}
        self._client.tx(xAxisTypeDict)
        # Set the window title
        titleDict = {Table2DPlotServer.WINDOW_TTLE: "IP Test"}
        self._client.tx(titleDict)
        # Set the table columns
        headerDict = {Table2DPlotServer.TABLE_COLUMNS: tableColNames}
        self._client.tx(headerDict)
        # Set the width of the result table
        headerDict = {Table2DPlotServer.RESULT_WIDTH: 75}
        self._client.tx(headerDict)
        self._checkForError()

class LogFileProcessor(Base):
    """@brief Responsible for parsing the data in a log file and forwarding it
              to the GUI."""
    LOG_FILE_PREFIX = "ipt_"
    CSV_SUFFIX = ".csv"

    def __init__(self, uio, options, guiLauncher):
        """@brief Constructor
           @param uio A UIO instance handling user input and output (E.G stdin/stdout or a GUI)
           @param options An instance of the OptionParser command line options.
           @param guiLauncher A GUILauncher instance."""
        super().__init__(uio, options)
        self._guiLauncher = guiLauncher
        self._log_fd = None

    def _is_valid_log(self, log_file):
        """@brief Determine if we have a valid log file."""
        valid_log_file = False
        filename = os.path.basename(log_file)
        if filename.startswith(LogFileProcessor.LOG_FILE_PREFIX) and\
           filename.endswith(LogFileProcessor.CSV_SUFFIX):
           valid_log_file = True

        return valid_log_file

    def new(self, log_file):
        """@brief Called when a new log file is created."""
        if self._is_valid_log(log_file):
            if self._log_fd is not None:
                self._log_fd.close()
                self._log_fd = None
            self.debug(f"Start reading from new log file: {log_file}")
            self._log_fd = open(log_file, 'r')
            lines = self._log_fd.readlines()
            for line in lines:
                self._guiLauncher.initGUI(line.split(","))

    def updated(self, log_file):
        """@brief Called when an existing log file is updated."""
        if self._is_valid_log(log_file):
            self.debug(f"Log file updated: {log_file}")
            lines = self._log_fd.readlines()
            for line in lines:
                self._guiLauncher.addResultSet(line.split(","))

class FolderChangeNotifier(Base):
    """@brief Responsible for notifying changes to a folder."""

    def __init__(self, uio, options, guiLauncher):
        """@brief Constructor
           @param uio A UIO instance handling user input and output (E.G stdin/stdout or a GUI)
           @param options An instance of the OptionParser command line options.
           @param guiLauncher A GUILauncher instance."""
        super().__init__(uio, options)
        self._guiLauncher = guiLauncher

        patterns = ["*"]
        ignore_patterns = None
        ignore_directories = False
        case_sensitive = True
        my_event_handler = PatternMatchingEventHandler(patterns, ignore_patterns, ignore_directories, case_sensitive)

        my_event_handler.on_created = self._on_created
        my_event_handler.on_deleted = self._on_deleted
        my_event_handler.on_modified = self._on_modified
        my_event_handler.on_moved = self._on_moved

        path = self._options.folder
        go_recursively = True
        self._my_observer = Observer()
        self._my_observer.schedule(my_event_handler, path, recursive=go_recursively)

    def _on_created(self, event):
        """@brief Called when a folder/file is created."""
        if os.path.isfile(event.src_path):
            self.logFileProcessor.new(event.src_path)

    def _on_deleted(self, event):
        """@brief Called when a folder/file is deleted."""
        pass

    def _on_modified(self, event):
        """@brief Called when a folder/file is modified."""
        if os.path.isfile(event.src_path):
            self.logFileProcessor.updated(event.src_path)

    def _on_moved(self, event):
        """@brief Called when a folder/file is moved."""
        pass

    def _plotFile(self, csvFile):
        """@brief Plot the contents of a CSV file.
           @param csvFile The ipt csv log file."""
        with open(csvFile, 'r') as fd:
            lines = fd.readlines()
        tableStart = False
        for line in lines:
            line = line.rstrip("\r\n")
            if not tableStart and line.startswith("#"):
                titleRow = line.split(",")
                self._guiLauncher.initGUI(titleRow)
                tableStart = True

            else:
                tableBodyRow = line.split(",")
                self._guiLauncher.addResultSet(tableBodyRow)

        # Stay running so that if the user selects the save html file button an HMTL file is saved.
        while True:
            sleep(1)



    def run(self, logFileProcessor):
        """@brief Blocking method to check for changes in the folder.
           @param logFileProcessor A LogFileProcessor instance."""
        if os.path.isfile(self._options.folder) and self._options.folder.endswith(".csv"):
            self._plotFile(self._options.folder)
        else:

            self._my_observer.start()
            self.logFileProcessor = logFileProcessor
            try:
                # do something else
                while True:
                    sleep(1)

            except KeyboardInterrupt:
                self._my_observer.stop()
                self._my_observer.join()
                raise

def main():
    """@brief Program entry point"""
    uio = UIO()

    try:
        parser = argparse.ArgumentParser(description="Provide a GUI interface to the ipt tool to show the data being plotted.",
                                         formatter_class=argparse.RawDescriptionHelpFormatter)

        parser.add_argument("-d",
                            "--debug",
                            action='store_true',
                            help="Enable debugging.")

        parser.add_argument("-f",
                            "--folder",
                            help="The folder to check for changes. This may also be an ipt csv log file. If so then it's contents will be plotted.",
                            default=os.path.join(str(Path.home()), "test_logs") )

        parser.add_argument("-o",
                           "--overlay",
                           action='store_true',
                           help="Rather than separate traces overlay them on the same plot area.")

        options = parser.parse_args()

        uio.enableDebug(options.debug)

        guiLauncher = GUILauncher(uio, options)
        logFileProcessor = LogFileProcessor(uio, options, guiLauncher)

        folderChangeNotifier = FolderChangeNotifier(uio, options, guiLauncher)
        folderChangeNotifier.run(logFileProcessor)

    #If the program throws a system exit exception
    except SystemExit:
        pass
    #Don't print error information if CTRL C pressed
    except KeyboardInterrupt:
        pass
    except Exception as ex:
        raise
        if options.debug:
            raise
        else:
            uio.error(str(ex))

if __name__== '__main__':
    main()
